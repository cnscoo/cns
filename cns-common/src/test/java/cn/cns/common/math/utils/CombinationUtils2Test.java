package cn.cns.common.math.utils;


import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.util.function.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CombinationUtils2Test {

    private List<Integer> numbers;

    @BeforeEach
    public void setUp() {
        numbers = new ArrayList<>();
    }

    @Test
    public void combinationsWithCondition_EmptyList_ReturnsEmptyGroupsAndRemaining() {
        Tuple2<List<List<Integer>>, List<Integer>> result = CombinationUtils2.combinationsWithCondition(numbers, 100);
        assertEquals(Collections.emptyList(), result.getT1());
        assertEquals(Collections.emptyList(), result.getT2());
    }

    @Test
    public void combinationsWithCondition_SumLessThan100_ReturnsEmptyGroupsAndRemaining() {
        numbers = Arrays.asList(10, 20, 30);
        Tuple2<List<List<Integer>>, List<Integer>> result = CombinationUtils2.combinationsWithCondition(numbers, 100);
        assertEquals(Collections.emptyList(), result.getT1());
        assertEquals(Arrays.asList(30, 20, 10), result.getT2());
    }

    @Test
    public void combinationsWithCondition_SumEquals100_ReturnsOneGroupAndEmptyRemaining() {
        numbers = Arrays.asList(50, 50);
        Tuple2<List<List<Integer>>, List<Integer>> result = CombinationUtils2.combinationsWithCondition(numbers, 100);
        assertEquals(Collections.singletonList(Arrays.asList(50, 50)), result.getT1());
        assertEquals(Collections.emptyList(), result.getT2());
    }

    @Test
    public void combinationsWithCondition_SumGreaterThan100_ReturnsGroupsAndRemaining() {
        numbers = Arrays.asList(50, 50, 10);
        Tuple2<List<List<Integer>>, List<Integer>> result = CombinationUtils2.combinationsWithCondition(numbers, 100);
        assertEquals(Collections.singletonList(Arrays.asList(50, 50)), result.getT1());
        assertEquals(Collections.singletonList(10), result.getT2());
    }

    @Test
    public void combinationsWithCondition_ExistsSubsetSum100_ReturnsGroupsAndRemaining() {
        numbers = Arrays.asList(10, 30, 30, 40);
        Tuple2<List<List<Integer>>, List<Integer>> result = CombinationUtils2.combinationsWithCondition(numbers, 100);
        assertEquals(Collections.singletonList(Arrays.asList(40, 30, 30)), result.getT1());
        assertEquals(Collections.singletonList(10), result.getT2());
    }

    @Test
    public void combinationsWithCondition_NoSubsetSum100_ReturnsEmptyGroupsAndRemaining() {
        numbers = Arrays.asList(10, 20, 30, 5);
        Tuple2<List<List<Integer>>, List<Integer>> result = CombinationUtils2.combinationsWithCondition(numbers, 100);
        assertEquals(Collections.emptyList(), result.getT1());
        assertEquals(Arrays.asList(30, 20, 10, 5), result.getT2());
    }

    @Test
    public void combinationsWithCondition_DuplicateNumbers_ReturnsGroupsAndRemaining() {
        numbers = Arrays.asList(50, 50, 60, 60);
        Tuple2<List<List<Integer>>, List<Integer>> result = CombinationUtils2.combinationsWithCondition(numbers, 100);
        assertEquals(Collections.singletonList(Arrays.asList(50, 50)), result.getT1());
        assertEquals(Arrays.asList(60, 60), result.getT2());
    }

    @Test
    public void combinationsWithCondition_MixedNumbers_ReturnsGroupsAndRemaining() {
        RandomUtils random = RandomUtils.secure();
        for (int i = 0; i < 30; i++) {
            numbers.add(random.randomInt(1, 100));
        }
        System.out.println("数组：" + numbers);

        System.out.println("===================================================");

        long t3 = System.nanoTime();
        Tuple2<List<int[]>, int[]> result2 = CombinationUtils.combinationsWithCondition(numbers, nums -> Arrays.stream(nums).sum() == 100);
        long t4 = System.nanoTime();
        System.out.println("组合列表：");
        for (int i = 0; i < result2.getT1().size(); i++) {
            System.out.printf("\t第 %d 组: %s\n", i + 1, Arrays.toString(result2.getT1().get(i)));
        }
        System.out.println("未能组合的数字: " + Arrays.toString(result2.getT2()));
        System.out.printf("共 %d 组，剩余 %d 个数字\n", result2.getT1().size(), result2.getT2().length);
        System.out.println("耗时2：" + (t4 - t3) / 1000 / 1000 + "ms");

        System.out.println("===================================================");

        long t1 = System.nanoTime();
        Tuple2<List<List<Integer>>, List<Integer>> result = CombinationUtils2.combinationsWithCondition(numbers, 100);
        long t2 = System.nanoTime();
        System.out.println("组合列表：");
        for (int i = 0; i < result.getT1().size(); i++) {
            System.out.printf("\t第 %d 组: %s\n", i + 1, result.getT1().get(i));
        }
        System.out.println("未能组合的数字: " + result.getT2());
        System.out.printf("共 %d 组，剩余 %d 个数字\n", result.getT1().size(), result.getT2().size());
        System.out.println("耗时1：" + (t2 - t1) / 1000 / 1000 + "ms");
    }
}
