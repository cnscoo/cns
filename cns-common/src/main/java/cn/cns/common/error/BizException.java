package cn.cns.common.error;

import cn.cns.common.dto.IMessage;
import cn.cns.common.dto.Result;
import lombok.Getter;
import lombok.Setter;

/**
 * @author dhc
 * 2019-11-21 00:14
 */
@Getter
@Setter
public class BizException extends RuntimeException {
    private int code;

    public BizException(int code, String message) {
        super(message);
        this.code = code;
    }

    public BizException(IMessage message) {
        super(message.getMessage());
        this.code = message.getCode();
    }

    public Result toResult() {
        return Result.error(this.code, this.getMessage());
    }
}
