package cn.cns.common.utils;

import reactor.util.function.Tuple3;
import reactor.util.function.Tuples;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 对象操作工具
 */
public class ObjectOperatorUtils {
    /**
     * Boolean 类型合并方式
     */
    public enum BoolMergeType {
        /**
         * 使用 &amp; 运算符合并
         */
        AND,
        /**
         * 使用 | 运算符合并
         */
        OR,
        ;

        public BinaryOperator<Long> merge() {
            return (a, b) -> {
                switch (this) {
                    case OR:
                        return a | b;
                    case AND:
                    default:
                        return a & b;
                }
            };
        }
    }

    /**
     * 合并多个对象的 Boolean 类型属性值
     *
     * @param objects        对象集合
     * @param boolFieldNames 需要合并的属性名称列表
     * @param type           合并方式
     * @param <T>            对象的类型
     * @return 属性合并后的对象
     */
    @SuppressWarnings("unchecked")
    public static <T> T boolFieldsMerge(Collection<T> objects, List<String> boolFieldNames, BoolMergeType type) {
        if (objects == null || objects.isEmpty() || boolFieldNames == null || boolFieldNames.isEmpty()) {
            return null;
        }
        Class<T> clz = (Class<T>) objects.iterator().next().getClass();
        Map<String, Field> fields = Arrays.asList(clz.getDeclaredFields()).parallelStream()
                .filter(f -> f.getType() == Boolean.class || f.getType() == boolean.class)
                .collect(Collectors.toMap(Field::getName, Function.identity()));
        Map<String, Method> getMethods = Arrays.stream(clz.getMethods())
                .filter(m -> m.getName().startsWith("get|is") && (m.getReturnType() == Boolean.class || m.getReturnType() == boolean.class) && m.getParameterCount() == 0)
                .collect(Collectors.toMap(Method::getName, Function.identity()));
        Long result = objects.parallelStream().map(t -> {
            String values = boolFieldNames.stream().map(i -> {
                try {
                    boolean bool = false;
                    Field field = fields.get(i);
                    if (field != null && field.canAccess(t)) {
                        bool = field.getBoolean(t);
                    } else {
                        String methodFieldName = i.substring(0, 1).toUpperCase() + i.substring(1);
                        Method method = Optional.ofNullable(getMethods.get("get" + methodFieldName)).orElse(getMethods.get("is" + methodFieldName));
                        if (method != null && method.canAccess(t)) {
                            bool = Optional.ofNullable((Boolean) method.invoke(t)).orElse(false);
                        }
                    }
                    return bool ? "1" : "0";
                } catch (IllegalAccessException | InvocationTargetException e) {
                    return "0";
                }
            }).collect(Collectors.joining());
            return Long.parseLong(values, Character.MIN_RADIX);
        }).reduce(type.merge()).orElse(0L);
        String binStr = Long.toBinaryString(result);
        int fills = boolFieldNames.size() - binStr.length();
        if (fills > 0) {
            binStr = Arrays.stream(new Byte[fills]).map(i -> "0").collect(Collectors.joining()) + binStr;
        }
        T t;
        try {
            Constructor<T> constructor = clz.getConstructor();
            if (!constructor.canAccess(null)) {
                throw new RuntimeException("类 " + clz.getName() + " 需要一个可访问的无参构造函数");
            }
            t = constructor.newInstance();
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("类 " + clz.getName() + " 需要一个可访问的无参构造函数");
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException("类 " + clz.getName() + " 使用无参构造函数实例化失败：" + e.getMessage());
        }
        Map<String, Method> setMethods = Arrays.stream(clz.getMethods())
                .filter(m -> m.getName().startsWith("set") && m.getParameterCount() == 1 && (m.getParameters()[0].getType().equals(Boolean.class) || m.getParameters()[0].getType().equals(boolean.class)))
                .collect(Collectors.toMap(Method::getName, Function.identity()));
        for (int i = 0; i < boolFieldNames.size(); i++) {
            boolean value = binStr.charAt(i) == '1';
            String fieldName = boolFieldNames.get(i);
            Field field = fields.get(fieldName);
            if (field != null && field.canAccess(t)) {
                try {
                    field.setBoolean(t, value);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else {
                String fieldSetMethodName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                Method method = Optional.ofNullable(setMethods.get(fieldName)).orElseThrow(() -> new RuntimeException("属性 " + clz.getName() + "#" + fieldName + " 不可访问，也没有对应的 set 方法，无法赋值。"));
                if (!method.canAccess(t)) {
                    throw new RuntimeException("属性 " + clz.getName() + "#" + fieldName + " 无法访问，对应的 set 方法也无法访问，导致无法赋值。");
                }
                try {
                    method.invoke(t, value);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new RuntimeException("方法 " + clz.getName() + "#" + fieldSetMethodName + " 调用失败: " + e.getMessage());
                }
            }
        }
        return t;
    }

    /**
     * 将新旧数据根据Id对比按照保存方式严格分组
     *
     * @param list     新数据
     * @param olds     就数据
     * @param idMapper id获取方式
     * @param <T>      数据类型
     * @param <R>      Id类型
     * @return 分组结果：需要增加的、需要更新的、需要删除的
     */
    public static <T, R> Tuple3<List<T>, List<T>, List<T>> toStrictSaveGroup(List<T> list, List<T> olds, Function<T, R> idMapper) {
        Set<R> oids = olds.isEmpty() ? Collections.emptySet() : olds.stream().map(idMapper).collect(Collectors.toSet());
        Set<R> nids = list.isEmpty() ? Collections.emptySet() : list.stream().map(idMapper).filter(Objects::nonNull).collect(Collectors.toSet());
        List<T> toDelete = list.isEmpty() ? olds : olds.stream().filter(i -> !nids.contains(idMapper.apply(i))).collect(Collectors.toList());
        if (!list.isEmpty()) {
            List<T> toUpdate = list.stream().filter(i -> idMapper.apply(i) != null && oids.contains(idMapper.apply(i))).collect(Collectors.toList());
            List<T> toInsert = list.stream().filter(i -> idMapper.apply(i) == null).collect(Collectors.toList());
            return Tuples.of(toInsert, toUpdate, toDelete);
        }
        return Tuples.of(new ArrayList<>(), new ArrayList<>(), toDelete);
    }
}
