package cn.cns.common.utils;

import java.util.Comparator;
import java.util.Objects;
import java.util.function.Function;

/**
 * 对象比较工具
 */
public class ComparatorUtils {

    /**
     * <p>空值比较，比较两个对象是否空值</p>
     * <p>如果A对象为null,B对象不为null，且 nullFirst=true,则返回-1，反之返回1</p>
     * <p>如果A对象不为null,B对象为null，且 nullFirst=true,则返回1，反之返回-1</p>
     * <p>如果两个对象都同时为null，则返回0</p>
     * <p>如果两个对象都同时不为null，则返回null</p>
     *
     * @param a         对象A
     * @param b         对象B
     * @param nullFirst 是否空值在前
     * @return 比较结果
     */
    public static Integer nullCompare(Object a, Object b, boolean nullFirst) {
        int i = nullFirst ? -1 : 1;
        if (a == null && b == null) {
            return 0;
        }
        if (a == null) {
            return i;
        }
        if (b == null) {
            return -i;
        }
        return null;
    }

    /**
     * 比较两个可为空对象
     *
     * @param a         对象A
     * @param b         对象B
     * @param nullFirst 是否空值在前
     * @param <T>       对象类型
     * @return 比较结果
     */
    public static <T extends Comparable<? super T>> int nullableCompare(T a, T b, boolean nullFirst) {
        Integer i = nullCompare(a, b, nullFirst);
        return i == null ? a.compareTo(b) : i;
    }

    /**
     * 接受一个函数，该函数从类型T中提取一个Comparable排序键，并返回一个Comparator<T>，该键通过该排序键进行比较。
     *
     * <p>排序键函数获取的值可以为空，根据参数 nullFirst 指定空值是否靠前。</p>
     *
     * @param keyExtractor 用于提取可Comparable排序键的函数
     * @param nullFirst    是否空值在前
     * @param <T>          需要比较的元素类型
     * @param <U>          排序键的类型
     * @return 通过提取的值进行比较的比较器
     */
    public static <T, U extends Comparable<? super U>> Comparator<T> nullableComparator(Function<? super T, ? extends U> keyExtractor, boolean nullFirst) {
        Objects.requireNonNull(keyExtractor);
        return (c1, c2) -> nullableCompare(keyExtractor.apply(c1), keyExtractor.apply(c2), nullFirst);
    }
}
