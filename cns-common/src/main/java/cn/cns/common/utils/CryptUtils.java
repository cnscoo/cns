package cn.cns.common.utils;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;

public class CryptUtils {

    private static Cipher buildCipher(String key, int opmode) throws GeneralSecurityException {
        byte[] keyBytes = key.getBytes(StandardCharsets.UTF_8);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
        IvParameterSpec iv = new IvParameterSpec(keyBytes, 0, 16);
        cipher.init(opmode, keySpec, iv);
        return cipher;
    }

    // AES 加密
    public static byte[] aesEncode(String txt, String key) throws GeneralSecurityException {
        byte[] txtBytes = txt.getBytes(StandardCharsets.UTF_8);
        return buildCipher(key, Cipher.ENCRYPT_MODE).doFinal(txtBytes);
    }

    // AES 加密
    public static String aesDecode(byte[] txtBytes, String key) throws GeneralSecurityException {
        byte[] decrypted = buildCipher(key, Cipher.DECRYPT_MODE).doFinal(txtBytes);
        return new String(decrypted, StandardCharsets.UTF_8);
    }

    // AES 加密
    public static String aesHexEncode(String txt, String key) throws GeneralSecurityException {
        return Hex.encodeHexString(aesEncode(txt, key));
    }

    // AES 解密
    public static String aesHexDecode(String txt, String key) throws GeneralSecurityException, DecoderException {
        return aesDecode(Hex.decodeHex(txt), key);
    }
}
