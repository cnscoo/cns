package cn.cns.common.utils;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

/**
 * 根据对象属性值列表顺序对对象进行比较
 *
 * @param <C> 属性值类型
 * @param <T> 对象类型
 */
public class ObjectFieldValueComparator<C, T> implements Comparator<T> {
    private final List<C> valueList;
    private final Function<T, C> valueMapper;
    private boolean noMatchLast;

    /**
     * 构建一个对象比较对象
     *
     * @param valueList   属性值列表，作为对象排序的目标方式
     * @param valueMapper 属性值获取方式
     * @param noMatchLast 未匹配到值的对象是否放在最后
     */
    public ObjectFieldValueComparator(List<C> valueList, Function<T, C> valueMapper, boolean noMatchLast) {
        this.valueMapper = valueMapper;
        this.valueList = valueList;
        this.noMatchLast = noMatchLast;
    }

    /**
     * 构建一个对象比较对象，未匹配到的值默认放在最后
     *
     * @param valueList   属性值列表，作为对象排序的目标方式
     * @param valueMapper 属性值获取方式
     * @param <C>         属性值类型
     * @param <T>         对象类型
     * @return 比较对象
     */
    public static <C, T> ObjectFieldValueComparator<C, T> newOf(List<C> valueList, Function<T, C> valueMapper) {
        return new ObjectFieldValueComparator<>(valueList, valueMapper, true);
    }

    /**
     * 构建一个对象比较对象
     *
     * @param valueList   属性值列表，作为对象排序的目标方式
     * @param valueMapper 属性值获取方式
     * @param noMatchLast 未匹配到值的对象是否放在最后
     * @param <C>         属性值类型
     * @param <T>         对象类型
     * @return 比较对象
     */
    public static <C, T> ObjectFieldValueComparator<C, T> newOf(List<C> valueList, Function<T, C> valueMapper, boolean noMatchLast) {
        return new ObjectFieldValueComparator<>(valueList, valueMapper, noMatchLast);
    }

    public List<C> getValueList() {
        return valueList;
    }

    public Function<T, C> getValueMapper() {
        return valueMapper;
    }

    public boolean isNoMatchLast() {
        return noMatchLast;
    }

    public void setNoMatchLast(boolean noMatchLast) {
        this.noMatchLast = noMatchLast;
    }

    @Override
    public int compare(T o1, T o2) {
        int i1 = valueList.indexOf(valueMapper.apply(o1));
        int i2 = valueList.indexOf(valueMapper.apply(o2));
        if (noMatchLast) {
            if (i1 == -1) {
                i1 = valueList.size();
            }
            if (i2 == -1) {
                i2 = valueList.size();
            }
        }
        return i1 - i2;
    }
}
