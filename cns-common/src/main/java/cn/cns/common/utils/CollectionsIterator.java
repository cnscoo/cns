package cn.cns.common.utils;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 集合分段器<br>
 * 将一个集合按照指定大小进行分段<br>
 *
 * @param <T> 此分段集合中元素的类型
 * @param <C> 集合的类型
 * @author dhc
 */
public class CollectionsIterator<T, C extends Collection<T>> implements Iterator<C> {
    private final C collection;
    private final Supplier<C> collectionFactory;
    private final int sectionSize;
    private final int sections;
    private final int mod;
    private int index = 0;


    /**
     * 构建一个集合分段器实例（仅为方便连写）
     *
     * @param collection  需要分段的集合
     * @param sectionSize 分段大小
     * @param <T>         此分段集合中元素的类型
     * @return CollectionsIterator&lt;T&gt; 分段器自身实例
     */
    public static <T> CollectionsIterator<T, List<T>> of(List<T> collection, int sectionSize) {
        return new CollectionsIterator<>(collection, sectionSize, false, ArrayList::new);
    }

    /**
     * 构建一个集合分段器实例（仅为方便连写）
     *
     * @param collection  需要分段的集合
     * @param sectionSize 分段大小
     * @param average     每段数量是否尽量平均
     * @param <T>         此分段集合中元素的类型
     * @return CollectionsIterator&lt;T&gt; 分段器自身实例
     */
    public static <T> CollectionsIterator<T, List<T>> of(List<T> collection, int sectionSize, Boolean average) {
        return new CollectionsIterator<>(collection, sectionSize, average, ArrayList::new);
    }

    /**
     * 构建一个集合分段器实例（仅为方便连写）
     *
     * @param collection  需要分段的集合
     * @param sectionSize 分段大小
     * @param <T>         此分段集合中元素的类型
     * @param <C>         集合的类型
     * @return CollectionsIterator&lt;T&gt; 分段器自身实例
     */
    public static <T, C extends Collection<T>> CollectionsIterator<T, C> of(C collection, int sectionSize, Supplier<C> collectionFactory) {
        return new CollectionsIterator<>(collection, sectionSize, false, collectionFactory);
    }

    /**
     * 构建一个集合分段器实例（仅为方便连写）
     *
     * @param collection  需要分段的集合
     * @param sectionSize 分段大小
     * @param average     每段数量是否尽量平均
     * @param <T>         此分段集合中元素的类型
     * @param <C>         集合的类型
     * @return CollectionsIterator&lt;T&gt; 分段器自身实例
     */
    public static <T, C extends List<T>> CollectionsIterator<T, C> of(C collection, int sectionSize, Boolean average, Supplier<C> collectionFactory) {
        return new CollectionsIterator<>(collection, sectionSize, average, collectionFactory);
    }

    /**
     * 构建一个集合分段器
     *
     * @param collection  需要分段的集合
     * @param sectionSize 分段大小
     */
    public CollectionsIterator(C collection, int sectionSize, Boolean average, Supplier<C> collectionFactory) {
        if (collection == null) {
            throw new NullPointerException("无法为一个 NULL 数据进行分段。");
        }
        if (collectionFactory == null) {
            throw new NullPointerException("集合提供者不能为空。");
        }
        if (sectionSize < 1) {
            throw new IllegalArgumentException("分段大小不能小于1。");
        }
        this.collection = collection;
        this.collectionFactory = collectionFactory;
        int count = this.collection.size();
        this.sections = (count - 1) / sectionSize + 1;
        if (average != null && average) {
            this.sectionSize = count / this.sections;
            this.mod = count % this.sections;
        } else {
            this.sectionSize = sectionSize;
            this.mod = 0;
        }
    }

    /**
     * 获取当前分段的索引位置
     *
     * @return {@code int} 分段器当前的索引位置
     */
    public int getIndex() {
        return index;
    }

    /**
     * 设置当前分段的索引位置
     *
     * @param index 索引位置
     * @return 当前对象
     */
    public CollectionsIterator<T, C> setIndex(int index) {
        if (index >= sections) {
            index = sections - 1;
        }
        if (index < 0) {
            index = 0;
        }
        this.index = index;
        return this;
    }

    /**
     * 获取分段器的分段大小
     *
     * @return {@code int} 分段器的分段大小
     */
    public int getSectionSize() {
        return sectionSize;
    }

    /**
     * 获取分段器的总分段数
     *
     * @return {@code int} 分段器的分段数量
     */
    public int getSections() {
        return sections;
    }

    /**
     * 重置分段器的索引位置为 0
     *
     * @return 重置后返回自身对象
     */
    public CollectionsIterator<T, C> reset() {
        this.index = 0;
        return this;
    }

    /**
     * 让分段器索引跳转指定的数量
     *
     * @param number 跳转的索引位置数量，负数往前跳转，正数往后跳转
     * @return {@code true} 跳转成功，{@code false} 如果目标位置超出索引范围
     */
    public boolean jump(int number) {
        return this.jumpTo(index + number);
    }

    /**
     * 将分段器索引跳转到指定位置
     *
     * @param index 索引位置，从 0 开始
     * @return {@code true} 跳转成功，{@code false} 如果目标位置超出索引范围
     */
    public boolean jumpTo(int index) {
        if (index >= 0 && index < sections) {
            this.index = index;
            return true;
        }
        return false;
    }

    /**
     * 判断是否还有下一段数据
     *
     * @return {@code true} 如果还有下一段数据
     */
    @Override
    public boolean hasNext() {
        return index < sections;
    }

    /**
     * 获取下一段数据
     *
     * @return {@link List} 下一分段的数据
     */
    @Override
    public C next() {
        return this.next(Collectors.toCollection(collectionFactory));
    }

    /**
     * 获取下一段数据，并自定义返回集合方式<br>
     *
     * @param collector 集合构建处理器
     * @param <R>       需要返回的集合类型
     * @param <A>       中间处理类型
     * @return Collection 下一段数据集合
     * @see Stream#collect(Collector)
     */
    public <R, A> R next(Collector<? super T, A, R> collector) {
        return this.nextSteam().collect(collector);
    }

    /**
     * 获取下一段数据，并将数据放入自行提供的集合对象<br>
     *
     * @param collectionFactory 集合构建器
     * @param <R>               需要返回的集合类型
     * @return Collection 下一段数据集合
     * @see Collectors#toCollection(Supplier)
     */
    public <R extends Collection<T>> R next(Supplier<R> collectionFactory) {
        return this.nextSteam().collect(Collectors.toCollection(collectionFactory));
    }

    /**
     * 获取下一段（串行）数据流
     *
     * @return {@link Stream}
     * @see Collection#stream()
     */
    public Stream<T> nextSteam() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        int skip = mod > 0 ? ((index * (sectionSize + 1)) - (index < mod ? 0 : index - mod)) : index * sectionSize;
        int limit = mod > 0 && index < mod ? sectionSize + 1 : sectionSize;
        index++;
        return collection.stream().skip(skip).limit(limit);
    }

    /**
     * 将分页集合通过自定义方法转换后合并为一个新的集合
     *
     * @param mapper 转换方法
     * @param <A>    集合数据类型
     * @param <N>    集合类型
     * @return 转换合并后的集合
     */
    public <A, N extends Collection<A>> N map(Function<C, N> mapper) {
        N n = null;
        while (this.hasNext()) {
            N sub = mapper.apply(this.next());
            if (n == null) {
                n = sub;
            } else {
                n.addAll(sub);
            }
        }
        return n;
    }

    /**
     * 将分页集合通过自定义整体转换方法转换后合并为一个新的集合
     *
     * @param mapper            分页集合转换方法
     * @param collectionFactory 集合构建器
     * @param <A>               集合数据泛型类型
     * @param <N>               集合对象类型
     * @return 转换合并后的集合
     */
    public <A, N extends Collection<A>> N map(Function<C, N> mapper, Supplier<N> collectionFactory) {
        N n = collectionFactory.get();
        while (this.hasNext()) {
            n.addAll(mapper.apply(this.next()));
        }
        return n;
    }

    /**
     * 将分页结果放入一个列表
     *
     * @return 分页数据列表
     */
    public List<C> toList() {
        List<C> result = new ArrayList<>(this.getSections());
        while (this.hasNext()) {
            result.add(this.next());
        }
        return result;
    }

    /**
     * 将分页集合通过自定义单个转换方法转换后合并为一个新的集合
     *
     * @param mapper            单个对象转换方法
     * @param collectionFactory 集合构建器
     * @param <A>               集合数据泛型类型
     * @param <N>               集合对象类型
     * @return 转换合并后的集合
     */
    public <A, N extends Collection<A>> N singleMap(Function<T, A> mapper, Supplier<N> collectionFactory) {
        N n = collectionFactory.get();
        while (this.hasNext()) {
            this.next().stream().map(mapper).forEach(n::add);
        }
        return n;
    }
}
