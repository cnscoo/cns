package cn.cns.common.utils;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 字符串扩展工具
 */
public class StringExtendUtils {

    /**
     * 字符串转下划线 CamelCase 转 snake_case
     *
     * @param in 输入字符串
     * @return 下划线字符串
     */
    public static String toSnakeCase(String in) {
        in = in.replaceAll(" +", "");
        StringBuilder result = new StringBuilder("" + Character.toLowerCase(in.charAt(0)));
        for (int i = 1; i < in.length(); i++) {
            char c = in.charAt(i);
            if (Character.isUpperCase(c)) {
                result.append("_").append(Character.toLowerCase(c));
            } else {
                result.append(c);
            }
        }
        return result.toString();
    }

    /**
     * 字符串转驼峰 snake_case 转 CamelCase
     *
     * @param in 输入字符串
     * @return 首字母大写的驼峰字符串
     */
    public static String toUpperFirstCamelCase(String in) {
        String[] tokens = in.split("_");
        StringBuilder camelCased = new StringBuilder();
        for (String token : tokens) {
            if (token.length() >= 1) {
                camelCased.append(token.substring(0, 1).toUpperCase()).append(token.substring(1));
            } else {
                camelCased.append("_");
            }
        }
        return camelCased.toString();
    }

    /**
     * 字符串转驼峰 snake_case 转 camelCase
     *
     * @param in 输入字符串
     * @return 首字母小写的驼峰字符串
     */
    public static String toLowerFirstCamelCase(String in) {
        String[] tokens = in.split("_");
        StringBuilder camelCased = new StringBuilder();
        for (int i = 1; i < tokens.length; i++) {
            String token = tokens[i];
            if (token.length() >= 1) {
                camelCased.append(token.substring(0, 1).toUpperCase()).append(token.substring(1));
            } else {
                camelCased.append("_");
            }
        }
        return camelCased.toString();
    }

    /**
     * 字符串填充
     *
     * @param str     需要填充的字符串
     * @param length  目标长度
     * @param cs      用于填充的字符串
     * @param toRight 是否填充在右边，否则填充在左边
     * @return String 填充好的字符串
     */
    public static String fill(String str, int length, String cs, boolean toRight) {
        int strLen = str == null ? 0 : str.length();
        if (strLen >= length || cs == null || cs.isEmpty()) {
            return str;
        }
        int shorts = length - strLen;
        int mod = shorts % cs.length();
        StringBuilder sb = new StringBuilder();
        sb.append(cs.repeat(shorts / cs.length()));
        if (mod != 0) {
            sb.append(cs, 0, mod);
        }
        String fills = sb.toString();
        return toRight ? str + fills : fills + str;
    }

    /**
     * 字符串填充比较
     *
     * @param s1        字符串1
     * @param s2        字符串2
     * @param c         填充字符
     * @param alignLeft 是否左对齐比较，否则右对齐比较
     * @return 比较后的值
     */
    public static int fillCompare(String s1, String s2, char c, boolean alignLeft) {
        int l1 = s1.length(), l2 = s2.length();
        if (l1 != l2) {
            if (l1 < l2) {
                s1 = fill(s1, l2, String.valueOf(c), alignLeft);
            } else {
                s2 = fill(s2, l1, String.valueOf(c), alignLeft);
            }
        }
        return s1.compareTo(s2);
    }

    /**
     * 从字符串中读取数字列表
     *
     * @param str 字符串，比如：1,5,7,9-18
     * @return 读取到的数字数组
     */
    public static List<Integer> readNumbers(String str) {
        return readNumbers(str, null);
    }

    /**
     * 从字符串中读取数字列表
     *
     * @param str   字符串，比如：1,5,7,9-18
     * @param range 限制读取到的范围
     * @return 读取到的数字数组
     */
    public static List<Integer> readNumbers(String str, Range<Integer> range) {
        if (StringUtils.isBlank(str)) {
            return Collections.emptyList();
        }
        IntStream stream = Arrays.stream(str.replaceAll("\\s", "").split(",")).map(i -> {
            if (i.matches("\\d+")) {
                return IntStream.of(Integer.parseInt(i));
            } else if (i.matches("\\d+-\\d+")) {
                Integer[] ints = Arrays.stream(i.split("-")).map(Integer::parseInt).sorted().toArray(Integer[]::new);
                return IntStream.rangeClosed(ints[0], ints[1]);
            }
            return null;
        }).filter(Objects::nonNull).flatMapToInt(Function.identity()).distinct();
        if (range != null) {
            stream = stream.filter(range::contains);
        }
        return stream.boxed().collect(Collectors.toList());
    }
}
