package cn.cns.common.utils;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.ClassMapBuilder;
import ma.glasnost.orika.metadata.Type;
import ma.glasnost.orika.metadata.TypeFactory;

import java.util.List;
import java.util.Map;

/**
 * 类型转换工具
 *
 * @author dhc
 * 2019-11-21 00:30
 */
public class BeanMapper {
    public static final MapperFactory MAPPER_FACTORY = new DefaultMapperFactory.Builder().build();
    private static final MapperFacade DEFAULT_MAPPER = MAPPER_FACTORY.getMapperFacade();


    /**
     * 简单的复制出新类型对象
     *
     * @param source    被复制的源对象
     * @param distClass 目标对象类型
     * @param <S>       源对象类型
     * @param <D>       目标对象类型
     * @return 复制后的对象
     */
    public static <S, D> D map(S source, Class<D> distClass) {
        return map(source, distClass, DEFAULT_MAPPER);
    }

    /**
     * 简单的复制出新类型对象
     *
     * @param source    被复制的对象
     * @param distClass 目标对象类型
     * @param mapper    复制支持工具
     * @param <S>       源对象类型
     * @param <D>       目标对象类型
     * @return 复制后的对象
     */
    public static <S, D> D map(S source, Class<D> distClass, MapperFacade mapper) {
        return mapper.map(source, distClass);
    }

    /**
     * 构建一个具有Class属性对应的 MapperFacade
     *
     * @param sourceClass 转入类型
     * @param distClass   转出类型
     * @param fieldMap    转入转出属性对照 Map
     * @param <S>         转入对象类型
     * @param <D>         转出对象类型
     * @return 构建后的 MapperFacade
     */
    private static <S, D> MapperFacade buildClassMapperFacade(Class<S> sourceClass, Class<D> distClass, Map<String, String> fieldMap) {
        MapperFactory factory = new DefaultMapperFactory.Builder().build();
        ClassMapBuilder<S, D> classMapBuilder = factory.classMap(sourceClass, distClass);
        fieldMap.keySet().forEach(k -> classMapBuilder.field(k, fieldMap.get(k)));
        classMapBuilder.byDefault().register();
        return factory.getMapperFacade();
    }

    /**
     * 根据字段映射转换对象
     *
     * @param source    转入对象
     * @param distClass 转出类型
     * @param fieldMap  字段映射
     * @param <S>       转入对象类型
     * @param <D>       转出对象类型
     * @return 转换后的对象
     */
    public static <S, D> D map(S source, Class<D> distClass, Map<String, String> fieldMap) {
        return buildClassMapperFacade(source.getClass(), distClass, fieldMap).map(source, distClass);
    }


    /**
     * 根据字段映射转换对象集合
     *
     * @param sourceList  转入对象集合
     * @param sourceClass 转入对象类型
     * @param distClass   转出类型
     * @param fieldMap    字段映射
     * @param <S>         转入对象类型
     * @param <D>         转出对象类型
     * @return 转换后的对象集合
     */
    public static <S, D> List<D> mapList(Iterable<S> sourceList, Class<S> sourceClass, Class<D> distClass, Map<String, String> fieldMap) {
        return buildClassMapperFacade(sourceClass, distClass, fieldMap).mapAsList(sourceList, TypeFactory.valueOf(sourceClass), TypeFactory.valueOf(distClass));
    }

    /**
     * 极致性能的复制出新类型对象.<br>
     * 预先通过BeanMapper.getType() 静态获取并缓存Type类型，在此处传入
     *
     * @param source     原始对象
     * @param sourceType 原始对象类型
     * @param distType   目标对象
     * @param <S>        原始对象类型
     * @param <D>        目标对象类型
     * @return 转换后的对象
     */
    public static <S, D> D map(S source, Type<S> sourceType, Type<D> distType) {
        return DEFAULT_MAPPER.map(source, sourceType, distType);
    }

    /**
     * 简单的复制出新对象列表到ArrayList<br>
     * 不建议使用，需要反射，实在有点慢
     *
     * @param sourceList  原始对象集合
     * @param sourceClass 原始对象类
     * @param distClass   目标对象类
     * @param <S>         原始对象类型
     * @param <D>         目标对象类型
     * @return 转换后的对象列表
     */
    public static <S, D> List<D> mapList(Iterable<S> sourceList, Class<S> sourceClass, Class<D> distClass) {
        return DEFAULT_MAPPER.mapAsList(sourceList, TypeFactory.valueOf(sourceClass), TypeFactory.valueOf(distClass));
    }

    /**
     * 极致性能的复制出新类型对象到ArrayList.<br>
     * 预先通过BeanMapper.getType() 静态获取并缓存Type类型，在此处传入
     *
     * @param sourceList      原始对象集合
     * @param sourceType      原始对象类型
     * @param destinationType 目标对象类型
     * @param <S>             原始对象类型
     * @param <D>             目标对象类型
     * @return 转换后的对象列表
     */
    public static <S, D> List<D> mapList(Iterable<S> sourceList, Type<S> sourceType, Type<D> destinationType) {
        return DEFAULT_MAPPER.mapAsList(sourceList, sourceType, destinationType);
    }

    /**
     * 简单复制出新对象列表到数组<br>
     * 通过 source.getComponentType() 获得源 Class
     *
     * @param destination      目标数组
     * @param source           原始数组
     * @param destinationClass 目标对象类型
     * @param <S>              原始对象类型
     * @param <D>              目标对象类型
     * @return 转换后的数组
     */
    public static <S, D> D[] mapArray(final D[] destination, final S[] source, final Class<D> destinationClass) {
        return DEFAULT_MAPPER.mapAsArray(destination, source, destinationClass);
    }

    /**
     * 极致性能的复制出新类型对象到数组<br>
     * 预先通过 BeanMapper.getType() 静态获取并缓存Type类型，在此处传入
     *
     * @param destination     目标数组
     * @param source          原始数组
     * @param sourceType      原始对象类型
     * @param destinationType 目标对象类型
     * @param <S>             原始对象类型
     * @param <D>             目标对象类型
     * @return 转换后的数组
     */
    public static <S, D> D[] mapArray(D[] destination, S[] source, Type<S> sourceType, Type<D> destinationType) {
        return DEFAULT_MAPPER.mapAsArray(destination, source, sourceType, destinationType);
    }
}
