package cn.cns.common.math.utils;

import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 数学组合工具
 */
public class CombinationUtils {
    /**
     * <p>根据指定条件获取数字分组</p>
     * <p>分组示例：分组的和等于 100</p>
     * <pre>
     * Random random = new Random();
     * List&lt;Integer&gt; nums = new ArrayList&lt;&gt;(100);
     * for (int i = 0; i &lt; 100; i++) {
     *     nums.add(random.nextInt(198) + 50);
     * }
     * Tuple2&lt;List&lt;int[]&gt;, int[]&gt; result = CombinationUtils.combinationsWithCondition(nums, ints -&gt; Arrays.stream(ints).sum() == 100);
     * </pre>
     *
     * @param digits    需要分组的数字
     * @param condition 分组条件
     * @return 分组结果和剩余数字
     */
    public static Tuple2<List<int[]>, int[]> combinationsWithCondition(Collection<Integer> digits, Function<int[], Boolean> condition) {
        return combinationsWithCondition(digits, -1, condition);
    }

    /**
     * <p>根据指定条件获取数字分组</p>
     *
     * @param digits    需要分组的数字
     * @param length    每个组的元素个数（小于1时自动分为 1-4 个元素一组），因性能问题，当数字个数大于 50 个时，尽量不要大于 5
     * @param condition 分组条件
     * @return 分组结果和剩余数字
     */
    public static Tuple2<List<int[]>, int[]> combinationsWithCondition(Collection<Integer> digits, int length, Function<int[], Boolean> condition) {
        List<Integer> nums = digits.stream().filter(Objects::nonNull).collect(Collectors.toList());
        if (nums.isEmpty()) {
            return Tuples.of(Collections.emptyList(), new int[0]);
        }
        if (nums.size() == 1) {
            int[] ints = {nums.get(0)};
            return condition.apply(ints) ? Tuples.of(Collections.singletonList(ints), new int[0]) : Tuples.of(Collections.emptyList(), ints);
        }
        List<int[]> list = new ArrayList<>();
        if (length > 0) {
            if (length > nums.size()) {
                return Tuples.of(Collections.emptyList(), nums.stream().mapToInt(i -> i).toArray());
            }
            if (length > 5) {
                System.err.println("警告：数字自由组合条件分组的元素个数应该尽量不大于 5，否则性能将受影响。");
            }
            combinationsSelect(nums, list, length, 0, condition);
        } else {
            for (int m = 1; m <= Math.min(nums.size(), 4); m++) {
                combinationsSelect(nums, list, m, 0, condition);
            }
        }
        return Tuples.of(list, nums.stream().mapToInt(Integer::intValue).toArray());
    }

    /**
     * 递归分组选择
     *
     * @param nums      分组数据
     * @param results   分组结果集
     * @param length    组内尺寸
     * @param start     开始位置
     * @param condition 分组条件
     * @param args      分组上级数组
     * @return 是否匹配
     */
    private static boolean combinationsSelect(List<Integer> nums, List<int[]> results, int length, int start, Function<int[], Boolean> condition, int... args) {
        if (length < 1 || length > nums.size()) {
            return false;
        }
        for (int i = start; i < nums.size(); i++) {
            int[] ints = new int[args.length + 1];
            System.arraycopy(args, 0, ints, 0, args.length);
            ints[args.length] = nums.get(i);
            if (ints.length == length) {
                if (condition.apply(ints)) {
                    results.add(ints);
                    for (int n : ints) {
                        nums.remove((Object) n);
                    }
                    return true;
                }
            } else {
                boolean match = combinationsSelect(nums, results, length, i + 1, condition, ints);
                if (match) {
                    if (args.length > 0) {
                        return true;
                    }
                    i--;
                }
            }
        }
        return false;
    }
}
