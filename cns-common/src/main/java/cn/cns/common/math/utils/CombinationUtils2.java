package cn.cns.common.math.utils;

import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CombinationUtils2 {

    /**
     * 在给定的列表中寻找一个子集，使其数字之和恰好为 target。
     * 如果找到，则返回该子集（一个新的 List），否则返回 null。
     */
    private static List<Integer> findSubset(List<Integer> nums, int target) {
        List<Integer> path = new ArrayList<>();
        if (backtrack(nums, target, 0, 0, path)) {
            // 返回 path 的一个拷贝，避免后续修改影响结果
            return new ArrayList<>(path);
        }
        return null;
    }

    /**
     * 回溯辅助方法：
     *
     * @param nums       待选数字列表
     * @param target     目标和（本题中为 100）
     * @param start      从列表的哪个位置开始选择
     * @param currentSum 当前已选数字之和
     * @param path       当前选择的数字序列
     * @return 如果找到满足条件的子集返回 true，否则返回 false
     */
    private static boolean backtrack(List<Integer> nums, int target, int start, int currentSum, List<Integer> path) {
        if (currentSum == target) {
            return true;
        }
        if (currentSum > target) {
            return false;
        }
        // 从 start 开始遍历列表中后续数字
        for (int i = start; i < nums.size(); i++) {
            int num = nums.get(i);
            path.add(num);
            if (backtrack(nums, target, i + 1, currentSum + num, path)) {
                return true;
            }
            // 回溯：撤销选择
            path.removeLast();
        }
        return false;
    }

    /**
     * 将数字列表反复划分成和为 100 的分组，返回分组结果及剩余未能组合的数字列表。
     *
     * @param numbers 待划分的数字列表
     * @return 元组对象，其中包含所有成功组合的分组 groups 和剩余数字 remaining
     */
    public static Tuple2<List<List<Integer>>, List<Integer>> combinationsWithCondition(List<Integer> numbers, int target) {
        // 为了剪枝效果更好，先将数字列表按降序排序
        numbers.sort(Collections.reverseOrder());
        List<List<Integer>> groups = new ArrayList<>();
        // 复制一份用于移除已经使用的数字
        List<Integer> remaining = new ArrayList<>(numbers);

        while (true) {
            // 在剩余数字中寻找和为 100 的子集
            List<Integer> subset = findSubset(remaining, target);
            if (subset == null) {
                break; // 无法再找到满足条件的子集，退出循环
            }
            groups.add(subset);
            // 从 remaining 中删除该子集中的每个数字（注意重复数字时只删除一次）
            for (Integer num : subset) {
                remaining.remove(num);
            }
        }
        return Tuples.of(groups, remaining);
    }
}
