package cn.cns.common.math.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class CombComputer {
    private static final Logger log = LoggerFactory.getLogger(CombComputer.class);

    private final int[] nums;   // 基础数字列表
    private final int targetValue; // 目标数字
    private final int n;
    private int groupValue = Integer.MIN_VALUE; // 临时组合
    private final boolean[][] mem; // 备忘录，默认值false
    private final List<Integer> things = new ArrayList<>();
    public List<List<Integer>> results = new ArrayList<>();
    private volatile List<List<Integer>> resultDistinct;
    // 状态：0未初始化，1已初始化，2计算中，3计算完成有结果，4计算完成无结果，5计算条件无结果
    private final AtomicInteger status = new AtomicInteger(0);
    private final boolean firstOne;

    public static CombComputer from(Collection<Integer> nums, int targetValue, boolean firstOne) {
        int[] ints = nums.stream().filter(Objects::nonNull).mapToInt(i -> i).toArray();
        return new CombComputer(ints, targetValue, firstOne);
    }

    public CombComputer(int[] nums, int targetValue, boolean firstOne) {
        this.nums = nums;
        this.n = nums.length;
        this.targetValue = targetValue;
        this.firstOne = firstOne;

        int total = Arrays.stream(nums).sum();
        if (targetValue == total) {
            results.add(Arrays.stream(nums).boxed().collect(Collectors.toList()));
            status.compareAndSet(0, 2);
            mem = null;
            return;
        }
        boolean match = targetValue < total;
        this.mem = match ? new boolean[n][targetValue + 1] : null;
        status.compareAndSet(0, match ? 1 : 5);
    }

    public CombComputer compute() {
        if (status.get() == 1) {
            status.compareAndSet(1, 2);
            doCompute(0, 0);
            status.compareAndSet(1, results.isEmpty() ? 4 : 3);
        } else {
            log.warn("组合计算器当前状态码为: {}, 不进行计算。", status.get());
        }
        return this;
    }

    private void doCompute(int i, int cw) {
        // cw==w表示装满了，i==n表示物品都考察完了
        if (cw == targetValue || i == n) {
            if (cw > groupValue) {
                groupValue = cw;
            }
            if (cw == targetValue) {
                results.add(new ArrayList<>(things));
            }
            return;
        }
        if (mem[i][cw]) {
            return; // 重复状态
        }
        mem[i][cw] = true; // 记录(i, cw)这个状态
        doCompute(i + 1, cw); // 选择不装第i个物品
        if (cw + nums[i] <= targetValue) { // 本次满足填充(cw + weight[i])且只找一组满足(maxW < w)
            if (!firstOne || groupValue < targetValue) {
                things.add(nums[i]);
                doCompute(i + 1, cw + nums[i]); // 选择装第i个物品
                things.remove(things.size() - 1);
            }
        }
    }

    public List<List<Integer>> distinct() {
        if (status.get() < 2 || results.isEmpty()) {
            return Collections.emptyList();
        }
        if (firstOne || results.size() == 1) {
            return results;
        }
        if (this.resultDistinct == null) {
            List<List<Integer>> dist = new ArrayList<>();
            Map<Integer, Integer> numCountMap = Arrays.stream(nums).boxed().collect(Collectors.groupingBy(i -> i, Collectors.mapping(i -> 1, Collectors.summingInt(o -> o))));
            Map<Integer, Integer> existsNumCountMap = new HashMap<>(numCountMap.size());
            for (List<Integer> result : results) {
                Map<Integer, Integer> resultNumCountMap = result.stream().collect(Collectors.groupingBy(i -> i, Collectors.mapping(i -> 1, Collectors.summingInt(o -> o))));
                if (resultNumCountMap.keySet().stream().anyMatch(i -> resultNumCountMap.get(i) + existsNumCountMap.getOrDefault(i, 0) > numCountMap.get(i))) {
                    continue;
                }
                resultNumCountMap.forEach((n, c) -> existsNumCountMap.compute(n, (k, v) -> v == null ? c : c + v));
                dist.add(result);
            }
            resultDistinct = dist;
        }
        return resultDistinct;
    }
}
