package cn.cns.common.dto;

/**
 * 返回消息体
 *
 * @author dhc
 * 2019-11-21 00:01
 */
public interface IMessage {

    /**
     * 消息代码
     *
     * @return 代码
     */
    int getCode();

    /**
     * 消息内容
     *
     * @return 消息内容
     */
    String getMessage();
}
