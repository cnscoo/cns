package cn.cns.common.dto;

import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 字典类型接口<br>
 * 如：用户状态（UserStatus）中1表示正常状态<br>
 * 则：value=1, label=正常
 *
 * @param <T> 字典类型
 */
public interface IDictionary<T extends Serializable> {

    //region 缓存属性映射
    Map<Class<? extends IDictionary<?>>, Map<Object, ? extends IDictionary<?>>> DICT_MAP = new HashMap<>(32);

    @SuppressWarnings("unchecked")
    static <T extends IDictionary<?>> T of(Class<? extends IDictionary<?>> clazz, Object value) {
        if (DICT_MAP.containsKey(clazz)) {
            return (T) DICT_MAP.get(clazz).get(value);
        }

        if (!clazz.isEnum()) return null;

        Map<Object, ? extends IDictionary<?>> map = Arrays.stream(clazz.getEnumConstants()).collect(Collectors.toMap(IDictionary::getValue, Function.identity()));
        DICT_MAP.put(clazz, map);
        return (T) map.get(value);
    }

    static <T extends IDictionary<?>> Optional<T> optOf(Class<? extends IDictionary<?>> clazz, Object value) {
        return Optional.ofNullable(of(clazz, value));
    }
    //endregion

    /**
     * 字典值
     *
     * @return 值
     */
    @JsonValue
    T getValue();

    /**
     * 字典标签
     *
     * @return 标签
     */
    String getLabel();

    /**
     * 判断值是否一致
     *
     * @param value 需要对比的值
     * @return boolean 是否匹配
     */
    default boolean is(T value) {
        return Objects.equals(this.getValue(), value);
    }

    /**
     * 获取多项字典的值列表
     *
     * @param e1 第一项
     * @param es 其他可选项
     * @return 字典项值集合
     */
    default List<T> valueList(IDictionary<T> e1, IDictionary<T>... es) {
        return Stream.concat(Stream.of(e1), Arrays.stream(es)).map(IDictionary::getValue).collect(Collectors.toList());
    }
}
