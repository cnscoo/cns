package cn.cns.common.dto;

import java.io.Serializable;
import java.util.function.Supplier;

/**
 * 分页参数对象
 *
 * @param <T> 分页查询对象类型
 */
public class PageParam<T extends Serializable> implements Serializable {
    // 页码
    private int page;
    //页尺寸
    private int size;
    //查询条件对象
    private T condition;

    public int getPage() {
        return Math.max(this.page, 1);
    }

    public void setPage(int page) {
        this.page = Math.max(page, 1);
    }

    public int getSize() {
        return this.size < 1 ? 20 : this.size;
    }

    public void setSize(int size) {
        this.size = size < 1 ? 20 : size;
    }

    public T getCondition() {
        return condition;
    }

    public void setCondition(T condition) {
        this.condition = condition;
    }

    /**
     * 获取查询条件对象
     *
     * @param defaultSupplier 如果对象为空时使用提供者给定的对象
     * @return 查询条件对象
     */
    public T getCondition(Supplier<T> defaultSupplier) {
        return condition == null ? defaultSupplier.get() : condition;
    }
}
