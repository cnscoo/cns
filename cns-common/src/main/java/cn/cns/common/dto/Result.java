package cn.cns.common.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 返回结果对象
 *
 * @author dhc
 * 2019-11-20 23:38
 */
@Data
public class Result implements Serializable {
    private int code = 1;
    private String message = "SUCCESS";
    private Object data;

    public Result() {
    }

    public Result(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public Result(IMessage message) {
        this.code = message.getCode();
        this.message = message.getMessage();
    }

    public Result(Object data) {
        this.data = data;
    }

    public Result(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 获取一个没有数据的成功结果
     *
     * @param 数据类型
     * @return 结果对象
     */
    public static Result success() {
        return new Result();
    }

    /**
     * 获取一个有数据的成功结果
     *
     * @param t 数据内容
     * @return 结果对象
     */
    public static Result success(Object t) {
        return new Result(t);
    }

    /**
     * 获取一个错误结果对象
     *
     * @param code    错误代码
     * @param message 错误消息
     * @param msgArgs 消息格式参数，如果不为空，则使用 {@link String#format(String, Object...)}格式化消息
     * @return 结果对象
     */
    public static Result error(int code, String message, Object... msgArgs) {
        String msg = message != null && message.length() > 1 && msgArgs != null && msgArgs.length > 0 ? String.format(message, msgArgs) : message;
        return new Result(code, msg);
    }

    /**
     * 获取一个错误结果对象
     *
     * @param message 错误消息体
     * @param msgArgs 消息格式参数，如果不为空，则使用 {@link String#format(String, Object...)}格式化消息体的消息
     * @return 结果对象
     */
    public static Result error(IMessage message, Object... msgArgs) {
        String msg = msgArgs != null && msgArgs.length > 0 ? String.format(message.getMessage(), msgArgs) : message.getMessage();
        return new Result(message.getCode(), msg);
    }
}
