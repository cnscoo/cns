package cn.cns.concurrent.task.group;

import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * 分组任务启动器<br>
 * 所有的任务根据分组Id进行分组<br>
 * 同一任务组在同一时间只有一个线程在执行<br>
 * 不同分组任务可以并行执行（取决于线程池设置）<br>
 * 任务组在任何时候都可以直接添加任务，如果正在执行中，则加入下一次执行的队列<br>
 *
 * @param <K> 分组Id类型
 * @param <T> 任务对象类型
 * @param <R> 任务结果对象类型
 * @author dhc
 */
public class GroupTaskRunner<K, T extends GroupTask<K>, R> {
    private final ExecutorService executor;
    private final BiFunction<K, T, R> function;
    private final Map<K, GroupTaskRunnable<K, T, R>> groups = new ConcurrentHashMap<>();

    /**
     * 根据线程数量构建一个任务启动器
     *
     * @param threads  线程数量
     * @param function 任务执行方法，该方法接收组 Id 和任务对象参数，需返回任务执行结果
     */
    public GroupTaskRunner(int threads, BiFunction<K, T, R> function) {
        int poolSize = Math.max(1, threads);
        this.executor = new ThreadPoolExecutor(poolSize, poolSize, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), new ThreadPoolExecutor.CallerRunsPolicy());
        this.function = function;
    }

    /**
     * 根据线程执行对象构建一个任务启动器
     *
     * @param executor 线程执行对象
     * @param function 任务执行方法，该方法接收组 Id 和任务对象参数，需返回任务执行结果
     */
    public GroupTaskRunner(ExecutorService executor, BiFunction<K, T, R> function) {
        this.executor = executor;
        this.function = function;
    }

    /**
     * 启动一个任务单
     *
     * @param order 任务单
     */
    private void run(GroupTaskOrder<K, T, R> order) {
        Map<K, List<GroupTaskUnit<K, T, R>>> taskMap = order.getTaskUnits().parallelStream().collect(Collectors.groupingBy(GroupTaskUnit::getGroupId, Collectors.toList()));
        taskMap.forEach((id, ts) -> {
            if (!groups.containsKey(id)) {
                groups.put(id, new GroupTaskRunnable<>(id, this.executor, function));
            }
            groups.get(id).addTasks(ts);
        });
    }

    /**
     * 同步启动执行任务列表
     *
     * @param tasks 任务列表
     * @return 执行完成的任务单（包含任务执行结果）
     */
    public GroupTaskOrder<K, T, R> runTasks(List<T> tasks) {
        if (tasks == null || tasks.isEmpty()) {
            return null;
        }
        Object lock = new Object();
        GroupTaskOrder<K, T, R> order = new GroupTaskOrder<>(tasks, groupTaskOrder -> {
            synchronized (lock) {
                lock.notify();
            }
        });
        this.run(order);
        try {
            synchronized (lock) {
                lock.wait();
            }
            return order;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 异步启动执行任务列表
     *
     * @param tasks    任务列表
     * @param callback 任务执行完成时的回调方法
     */
    public void runTasksAsync(List<T> tasks, Consumer<GroupTaskOrder<K, T, R>> callback) {
        if (tasks == null || tasks.isEmpty()) {
            callback.accept(null);
            return;
        }
        this.run(new GroupTaskOrder<>(tasks, callback));
    }

    /**
     * 启动分组任务执行器
     *
     * @param runnable 任务执行器
     */
    void execute(GroupTaskRunnable<K, T, R> runnable) {
        this.executor.execute(runnable);
    }

    /**
     * 关闭任务启动器，关闭后该启动器无法再次执行
     */
    public void shutdown() {
        this.executor.shutdown();
    }
}
