package cn.cns.concurrent.task.group;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.function.BiFunction;

/**
 * 任务执行器
 *
 * @param <K> 分组Id类型
 * @param <T> 任务对象类型
 * @param <R> 任务结果对象类型
 * @author dhc
 */
public class GroupTaskRunnable<K, T extends GroupTask<K>, R> implements Runnable {
    private final K id;
    private boolean inQueue = false;
    private final BiFunction<K, T, R> function;
    private final ExecutorService executor;
    private final List<GroupTaskUnit<K, T, R>> pendingTasks = new ArrayList<>();
    private final List<GroupTaskUnit<K, T, R>> executeTasks = new ArrayList<>();

    /**
     * 构建一个任务执行器
     *
     * @param id       任务分组Id
     * @param executor 任务执行器
     * @param function 任务执行方法
     */
    public GroupTaskRunnable(K id, ExecutorService executor, BiFunction<K, T, R> function) {
        this.id = id;
        this.executor = executor;
        this.function = function;
    }

    /**
     * 获取任务分组的Id
     *
     * @return 任务分组Id
     */
    public K getId() {
        return id;
    }

    /**
     * 如果执行器不在执行队列中则将执行器添加到队列
     */
    private void queue() {
        if (!this.inQueue) {
            this.inQueue = true;
            this.executor.execute(this);
        }
    }

    /**
     * 添加任务，如果执行器没有在执行过程中，则会将任务添加的执行队列
     *
     * @param task 添加的任务
     */
    public void addTask(GroupTaskUnit<K, T, R> task) {
        if (task == null) {
            return;
        }
        synchronized (this) {
            this.pendingTasks.add(task);
            this.queue();
        }
    }

    /**
     * 批量添加任务，如果执行器没有在执行过程中，则会将任务添加的执行队列
     *
     * @param tasks 添加的任务
     */
    public void addTasks(List<GroupTaskUnit<K, T, R>> tasks) {
        if (tasks == null || tasks.isEmpty()) {
            return;
        }
        synchronized (this) {
            this.pendingTasks.addAll(tasks);
            this.queue();
        }
    }

    /**
     * 判断执行器是否在执行中
     *
     * @return 是否在执行中
     */
    public boolean isRunning() {
        return !this.executeTasks.isEmpty();
    }

    /**
     * 判断执行器是否有任务在等待执行
     *
     * @return 是否后任务在等待执行
     */
    public boolean isPending() {
        return !this.pendingTasks.isEmpty();
    }

    /**
     * 执行任务
     */
    @Override
    public void run() {
        synchronized (this) {
            this.inQueue = false;
            if (!this.isPending() || this.isRunning()) {
                return;
            }
            this.executeTasks.addAll(this.pendingTasks);
            this.pendingTasks.clear();
        }
        this.executeTasks.parallelStream().forEach(t -> {
            try {
                t.callback(function.apply(this.getId(), t.getTask()));
            } catch (Exception e) {
                t.callbackError(e);
            }
        });
        synchronized (this) {
            this.executeTasks.clear();
        }
    }
}
