package cn.cns.concurrent.task.group;

import java.util.function.Consumer;

/**
 * 分组任务单元对象<br>
 * 用于承载任务对象，并且记录在任务执行过程中的异常
 *
 * @param <K> 分组Id类型
 * @param <T> 任务对象类型
 * @param <R> 任务结果对象类型
 */
class GroupTaskUnit<K, T extends GroupTask<K>, R> {
    private final K groupId;
    private final T task;
    private final Consumer<R> callback;
    private R result;
    private Exception exception;

    /**
     * 构造一个任务单元
     *
     * @param task     任务对象
     * @param callback 任务结果回调
     */
    public GroupTaskUnit(T task, Consumer<R> callback) {
        this.task = task;
        this.groupId = task.getGroupId();
        this.callback = callback;
    }

    /**
     * 获取任务分组的Id
     *
     * @return 任务分组Id
     */
    public K getGroupId() {
        return groupId;
    }

    /**
     * 获取任务对象
     *
     * @return 任务对象
     */
    public T getTask() {
        return task;
    }

    /**
     * 任务结果回调
     *
     * @param result 任务执行结果对象
     */
    void callback(R result) {
        this.result = result;
        this.callback.accept(result);
    }

    /**
     * 获取执行结果
     *
     * @return 执行结果
     */
    public R getResult() {
        return result;
    }

    /**
     * 任务错误回调
     *
     * @param e 错误异常对象
     */
    public void callbackError(Exception e) {
        this.exception = e;
        this.callback.accept(null);
    }

    /**
     * 获取异常对象
     *
     * @return 异常对象
     */
    public Exception getException() {
        return exception;
    }

    /**
     * 判断任务是否有异常
     *
     * @return 是否有异常
     */
    public boolean hasError() {
        return this.exception != null;
    }
}
