package cn.cns.concurrent.task.group;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * 任务单<br>
 * 一个任务单中包含多个任务，任务单中的任务是原始任务，没有经过处理和分组
 *
 * @param <K> 分组Id类型
 * @param <T> 任务对象类型
 * @param <R> 任务结果对象类型
 * @author dhc
 */
public class GroupTaskOrder<K, T extends GroupTask<K>, R> {
    private final List<GroupTaskUnit<K, T, R>> tasks;
    private final List<R> results;
    private boolean isFinish = false;
    private final AtomicInteger deals = new AtomicInteger(0);
    private final Consumer<GroupTaskOrder<K, T, R>> finishCallback;

    /**
     * 构建一个任务单
     *
     * @param tasks          任务列表
     * @param finishCallback 任务执行完成后的回调（包含执行错误）
     */
    public GroupTaskOrder(List<T> tasks, Consumer<GroupTaskOrder<K, T, R>> finishCallback) {
        this.results = new ArrayList<>(tasks.size());
        Consumer<R> dealt = this::onDealt;
        this.tasks = tasks.parallelStream().map(t -> new GroupTaskUnit<>(t, dealt)).collect(Collectors.toList());
        this.finishCallback = finishCallback;
    }

    /**
     * 当有任务执行完成时（包含执行错误）
     *
     * @param result 任务执行结果对象（执行错误时为 null）
     */
    private void onDealt(R result) {
        if (result != null) {
            this.results.add(result);
        }
        int deals = this.deals.incrementAndGet();
        if (deals >= this.tasks.size()) {
            this.isFinish = true;
            this.finishCallback.accept(this);
        }
    }

    /**
     * 判断任务单中是否有任务执行错误
     *
     * @return 是否有执行错误
     */
    public boolean hasError() {
        return this.tasks.parallelStream().anyMatch(GroupTaskUnit::hasError);
    }

    /**
     * 获取所有执行错误
     *
     * @return 由任务对象和异常对象组成的键值对象
     */
    public Map<T, Exception> getErrors() {
        return this.tasks.parallelStream().filter(GroupTaskUnit::hasError).collect(Collectors.toMap(GroupTaskUnit::getTask, GroupTaskUnit::getException));
    }

    /**
     * 判断需要执行的任务是否为空
     *
     * @return 任务是否为空
     */
    public boolean isEmpty() {
        return this.tasks == null || this.tasks.isEmpty();
    }

    /**
     * 获取所有的任务单元
     *
     * @return 任务单元列表
     */
    List<GroupTaskUnit<K, T, R>> getTaskUnits() {
        return tasks;
    }

    /**
     * 任务单是否执行完成（含执行错误）
     *
     * @return 是否完成
     */
    public boolean isFinish() {
        return isFinish;
    }

    /**
     * 获取任务单的执行结果
     *
     * @return 任务结果对象列表
     */
    public List<R> getResults() {
        return results;
    }
}
