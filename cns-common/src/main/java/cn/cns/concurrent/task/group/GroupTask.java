package cn.cns.concurrent.task.group;

/**
 * 分组任务对象
 *
 * @param <K> 组Id类型
 * @author dhc
 */
public interface GroupTask<K> {
    /**
     * 获取分组任务的组Id
     *
     * @return 分组Id
     */
    K getGroupId();
}
