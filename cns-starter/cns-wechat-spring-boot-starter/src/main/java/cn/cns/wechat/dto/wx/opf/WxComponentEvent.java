package cn.cns.wechat.dto.wx.opf;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * @author dhc
 * 2019-05-30 09:59
 */
@Data
@JacksonXmlRootElement(localName = "xml")
@JsonIgnoreProperties(ignoreUnknown = true)
public class WxComponentEvent {
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "AppId")
    private String appId;
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "AuthorizerAppid")
    private String authorizerAppId;
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "AuthorizationCode")
    private String authorizationCode;
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "AuthorizationCodeExpiredTime")
    private Long authorizationCodeExpiredTime;
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "PreAuthCode")
    private String preAuthCode;
    @JacksonXmlProperty(localName = "CreateTime")
    private Long createTime;
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "InfoType")
    private String infoType;
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "ComponentVerifyTicket")
    private String componentVerifyTicket;
    @JacksonXmlProperty(localName = "appid")
    private String mpAppId;
    @JacksonXmlProperty(localName = "status")
    private Integer status;
    @JacksonXmlProperty(localName = "auth_code")
    private String authCode;
    @JacksonXmlProperty(localName = "info")
    private Info info;

    public LocalDateTime getCreateDateTime() {
        if (this.createTime == null) {
            return null;
        }
        return LocalDateTime.ofEpochSecond(this.createTime, 0, ZoneOffset.ofHours(8));
    }

    public LocalDateTime getAuthorizationCodeExpiredDateTime() {
        if (this.authorizationCodeExpiredTime == null) {
            return null;
        }
        return LocalDateTime.ofEpochSecond(this.authorizationCodeExpiredTime, 0, ZoneOffset.ofHours(8));
    }

    @Data
    public static class Info {
        // 企业名称
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "name")
        // 企业代码
        private String name;
        // 代码类型
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "code")
        private String code;
        @JacksonXmlProperty(localName = "code_type")
        private Integer codeType;
        // 法人微信号
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "legal_persona_wechat")
        private String legalPersonaWechat;
        // 法人姓名
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "legal_persona_name")
        private String legalPersonaName;
        // 第三方联系电话
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "component_phone")
        private String componentPhone;
    }
}
