package cn.cns.wechat.dto.wx;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author dhc
 * 2019-05-30 13:21
 */
@Data
public class WxApiBack implements Serializable {
    @JsonProperty("errcode")
    private Integer errCode;
    @JsonProperty("errmsg")
    private String errMsg;

    public boolean hasError() {
        return this.errCode != null && this.errCode > 0;
    }
}
