package cn.cns.wechat.dto.wx.opf.params;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 获取授权方的帐号基本信息的参数
 *
 * @author dhc
 * 2019-05-30 14:01
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxComponentAuthorizerParam extends WxApiComponentParam {
    @JsonProperty("authorizer_appid")
    private String authorizerAppid;

    public WxComponentAuthorizerParam(String appId, String authorizerAppid) {
        super(appId);
        this.authorizerAppid = authorizerAppid;
    }
}
