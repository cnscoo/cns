package cn.cns.wechat.dto.wx.opf;

import cn.cns.wechat.dto.wx.WxApiBack;
import cn.cns.wechat.dto.wx.ext.AuthorizationInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dhc
 * 2019-05-30 13:47
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxComponentAuthorization extends WxApiBack {
    @JsonProperty("authorization_info")
    private AuthorizationInfo authorizationInfo;
}
