package cn.cns.wechat.controller;

import cn.cns.web.utils.ServletUtils;
import cn.cns.wechat.dto.wx.WxEncryptInfo;
import cn.cns.wechat.dto.wx.gzh.WxGzhAesMessage;
import cn.cns.wechat.service.WechatOpenPlatformMessageService;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author dhc
 * 2019-11-22 00:16
 */
@ResponseBody
public class WechatPlatformController {
    private final WechatOpenPlatformMessageService service;

    public WechatPlatformController(WechatOpenPlatformMessageService service) {
        this.service = service;
    }

    /**
     * 接收微信公众平台定时消息
     *
     * @param encryptInfo 定时消息内容
     * @return 返回 “success” 表示接收成功
     */
    public String postEvents(@RequestBody WxEncryptInfo encryptInfo) {
        service.onComponentTicketEvent(encryptInfo);
        return "success";
    }

    /**
     * 接收并应答微信公众平台代理的公众号消息
     *
     * @param appid      公众号 AppId
     * @param aesMessage 加密的公众号消息
     * @param response   Http请求响应对象
     */
    public void postMessage(@PathVariable String appid, @RequestBody WxGzhAesMessage aesMessage, HttpServletResponse response) {
        WxGzhAesMessage back = service.onComponentGzhAesMessage(appid, aesMessage);
        if (back == null) {
            ServletUtils.responseText(response, "");
        } else {
            ServletUtils.responseXml(response, back);
        }
    }

}
