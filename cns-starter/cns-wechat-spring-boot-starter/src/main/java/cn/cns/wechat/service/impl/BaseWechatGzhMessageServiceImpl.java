package cn.cns.wechat.service.impl;

import cn.cns.web.utils.BeanCache;
import cn.cns.wechat.dto.wx.gzh.WxGzhAesMessage;
import cn.cns.wechat.dto.wx.gzh.WxGzhMessage;
import cn.cns.wechat.dto.wx.gzh.WxMsgType;
import cn.cns.wechat.itfs.WechatMessageEvent;
import cn.cns.wechat.utils.WxCrypter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.qq.weixin.mp.aes.AesException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Calendar;

import static cn.cns.wechat.dto.wx.gzh.WxMsgType.TEXT;

/**
 * @author dhc
 * 2019-12-17 23:00
 */
@Slf4j
public abstract class BaseWechatGzhMessageServiceImpl {
    protected WechatMessageEvent event;

    protected BaseWechatGzhMessageServiceImpl(WechatMessageEvent event) {
        this.event = event;
    }

    protected WxGzhAesMessage answerAesMessage(String appId, String encodingAesKey, String token, WxGzhAesMessage aesMessage) {
        WxGzhMessage message = null, backMessage = null;
        try {
            String decrypt = WxCrypter.decrypt(appId, encodingAesKey, aesMessage.getEncrypt());
            log.debug("WX >>> 收到加密消息：\n{}", decrypt);
            message = BeanCache.getXmlMapper().readValue(decrypt, WxGzhMessage.class);
        } catch (AesException e) {
            log.error("WX >>> 解密微信加密消息内容失败，Code: " + e.getCode() + ", Message: " + e.getMessage());
            backMessage = event.onDecodeAesMessageException(aesMessage, e);
        } catch (JsonProcessingException e) {
            log.error("WX >>> 反序列化微信加密消息内容失败", e);
            return null;
        }
        if (backMessage == null && message != null) {
            backMessage = this.answerMessage(message);
        }
        if (backMessage == null) {
            return null;
        }

        String backString;
        try {
            backString = BeanCache.getXmlMapper().writeValueAsString(backMessage);
        } catch (JsonProcessingException e) {
            log.error("WX >>> 序列化微信回答消息内容失败", e);
            return null;
        }
        try {
            String nonce = RandomStringUtils.randomAlphanumeric(16);
            String timeStamp = String.valueOf(Calendar.getInstance().getTimeInMillis() / 1000);
            String encrypt = WxCrypter.encrypt(appId, encodingAesKey, nonce, backString);
            String msgSignature = WxCrypter.sha1(token, timeStamp, nonce, encrypt);

            WxGzhAesMessage aesBackMessage = new WxGzhAesMessage();
            aesBackMessage.setNonce(nonce);
            aesBackMessage.setTimeStamp(timeStamp);
            aesBackMessage.setEncrypt(encrypt);
            aesBackMessage.setMsgSignature(msgSignature);
            return aesBackMessage;
        } catch (AesException e) {
            log.error("WX >>> 加密微信回答消息失败，Code: " + e.getCode() + ", Message: " + e.getMessage(), e);
            return null;
        }
    }

    public WxGzhMessage answerMessage(WxGzhMessage message) {
        String fromUser = message.getFromUserName(), toUser = message.getToUserName();

        WxGzhMessage back = event.onMessage(message);
        if (back != null) {
            WxMsgType backType = WxMsgType.of(back.getMsgType()).orElse(null);
            if (backType == null || !backType.answer) {
                back.setMsgType(TEXT.code);
            }
            back.setFromUserName(toUser);
            back.setToUserName(fromUser);
            back.setCreateTime(Calendar.getInstance().getTimeInMillis() / 1000);
        }
        return back;
    }
}
