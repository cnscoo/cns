package cn.cns.wechat.dto.wx.gzh;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dhc
 * 2017-12-26 15:12
 */
@Data
@JacksonXmlRootElement(localName = "xml")
@JsonIgnoreProperties(ignoreUnknown = true)
public class WxGzhMessage implements Serializable {
    /**
     * 加密的消息内容
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Encrypt")
    private String encrypt;

    /**
     * 消息ID
     */
    @JacksonXmlProperty(localName = "MsgId")
    private Long msgId;

    /**
     * 目标用户
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "ToUserName")
    private String toUserName;

    /**
     * 发送用户
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "FromUserName")
    private String fromUserName;

    /**
     * 消息类型
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "MsgType")
    private String msgType;

    /**
     * 创建时间
     */
    @JacksonXmlProperty(localName = "CreateTime")
    private Long createTime;

    /**
     * 通用多媒体ID
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "MediaId")
    private String mediaId;

    /**
     * 文本消息
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Content")
    private String content;

    /**
     * 图片消息
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "PicUrl")
    private String picUrl;

    /**
     * 语音消息.媒体格式
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Format")
    private String format;

    /**
     * 语音消息.语音识别结果，UTF8编码
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Recognition")
    private String recognition;

    /**
     * 视频消息.视频缩略图
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "ThumbMediaId")
    private String thumbMediaId;

    /**
     * 位置消息.X坐标
     */
    @JacksonXmlProperty(localName = "Location_X")
    private Double locationX;

    /**
     * 位置消息.Y坐标
     */
    @JacksonXmlProperty(localName = "Location_Y")
    private Double locationY;

    @JacksonXmlProperty(localName = "Scale")
    private Integer scale;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Label")
    private String label;

    /**
     * 链接消息
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Title")
    private String title;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Description")
    private String description;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Url")
    private String url;

    /**
     * 事件消息
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Event")
    private String event;

    /**
     * 事件 Key
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "EventKey")
    private String eventKey;

    /**
     * 被点击自定义菜单的 Id
     */
    @JacksonXmlProperty(localName = "MenuId")
    private String menuId;

    /**
     * 二维码的ticket，可用来换取二维码图片
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Ticket")
    private String ticket;

    // 向微信发送消息时的对象 ▼

    /**
     * 扫描二维码的结果
     */
    @JacksonXmlProperty(localName = "ScanCodeInfo")
    private ScanCodeInfo scanCodeInfo;

    /**
     * 图片选择结果
     */
    @JacksonXmlProperty(localName = "SendPicsInfo")
    private SendPicsInfo sendPicsInfo;

    /**
     * 弹出地理位置选择器的消息内容
     */
    @JacksonXmlProperty(localName = "SendLocationInfo")
    private SendLocationInfo sendLocationInfo;
    /**
     * 发送的图片对象
     */
    @JacksonXmlProperty(localName = "Image")
    private Image image;

    /**
     * 发送的声音对象
     */
    @JacksonXmlProperty(localName = "Voice")
    private Voice voice;

    /**
     * 发送的视频对象
     */
    @JacksonXmlProperty(localName = "Video")
    private Video video;

    /**
     * 发送的音乐对象
     */
    @JacksonXmlProperty(localName = "Music")
    private Music music;

    /**
     * 发送的图文列表
     */
    @JacksonXmlProperty(localName = "item")
    @JacksonXmlElementWrapper(localName = "Articles")
    private List<Article> articles;

    public void setImage(String mediaId) {
        if (this.image == null) {
            this.image = new Image();
        }
        this.image.setMediaId(mediaId);
    }

    public void setVoice(String mediaId) {
        if (this.voice == null) {
            this.voice = new Voice();
        }
        this.voice.setMediaId(mediaId);
    }

    public void setVideo(String mediaId, String title, String description) {
        if (this.video == null) {
            this.video = new Video();
        }
        video.setMediaId(mediaId);
        video.setTitle(title);
        video.setDescription(description);
    }


    public void setMusic(String title, String description, String musicUrl, String hqMusicUrl, String thumbMediaId) {
        if (this.music == null) {
            this.music = new Music();
        }
        this.music.setTitle(title);
        this.music.setDescription(description);
        this.music.setMusicUrl(musicUrl);
        this.music.setHqMusicUrl(hqMusicUrl);
        this.music.setThumbMediaId(thumbMediaId);
    }

    public void addArticle(Article article) {
        if (this.articles == null) {
            this.articles = new ArrayList<>();
        }
        this.articles.add(article);
    }

    public void addArticle(String title, String description, String picUrl, String url) {
        this.addArticle(new Article(title, description, picUrl, url));
    }

    @JacksonXmlProperty(localName = "ArticleCount")
    public Integer getArticleCount() {
        return this.articles == null ? null : this.articles.size();
    }

    @Data
    public static class Image {
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "MediaId")
        private String mediaId;
    }

    @Data
    public static class Voice {
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "MediaId")
        private String mediaId;
    }

    @Data
    public static class Video {
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "MediaId")
        private String mediaId;
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "Title")
        private String title;
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "Description")
        private String description;
    }

    @Data
    public static class Music {
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "Title")
        private String title;
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "Description")
        private String description;
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "MusicUrl")
        private String musicUrl;
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "HQMusicUrl")
        private String hqMusicUrl;
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "ThumbMediaId")
        private String thumbMediaId;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @JacksonXmlRootElement(localName = "item")
    public static class Article {
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "Title")
        private String title;
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "Description")
        private String description;
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "PicUrl")
        private String picUrl;
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "Url")
        private String url;
    }

    @Data
    public static class ScanCodeInfo {
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "ScanType")
        private String scanType;
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "ScanResult")
        private String scanResult;
    }

    /**
     * 发送的图片信息
     */
    @Data
    public static class SendPicsInfo {
        @JacksonXmlProperty(localName = "Count")
        private Integer count;
        @JacksonXmlElementWrapper(localName = "PicList")
        @JacksonXmlProperty(localName = "item")
        private List<PicListItem> picList;
    }

    /**
     * 发送的图片对象
     */
    @Data
    public static class PicListItem {
        /**
         * 图片的MD5值，可用于验证接收到图片
         */
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "PicMd5Sum")
        private String picMd5Sum;
    }

    /**
     * 理位置选择器内容对象
     */
    @Data
    public static class SendLocationInfo {
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "Location_X")
        private double locationX;
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "Location_Y")
        private double locationY;
        /**
         * 精度，可理解为精度或者比例尺、越精细的话 scale越高
         */
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "Scale")
        private Integer scale;
        /**
         * 地理位置信息
         */
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "Label")
        private String label;
        /**
         * 朋友圈POI的名字，可能为空
         */
        @JacksonXmlCData
        @JacksonXmlProperty(localName = "Poiname")
        private String poiname;
    }
}
