package cn.cns.wechat.dto;

import java.time.LocalDateTime;

/**
 * @author dhc
 * 2019-06-02 20:48
 */
public interface Expires {
    /**
     * 多少秒之后过期
     *
     * @return 过期时长
     */
    Integer getExpiresIn();

    /**
     * 设置过期时长
     *
     * @param expiresIn 过期时长
     */
    void setExpiresIn(Integer expiresIn);

    /**
     * 获取过期时长对应的时间（默认减去60秒）
     *
     * @return 过期时间
     */
    default LocalDateTime getExpiredTime() {
        if (this.getExpiresIn() == null) {
            return null;
        }
        return LocalDateTime.now().plusSeconds(this.getExpiresIn() - 60);
    }
}
