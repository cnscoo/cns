package cn.cns.wechat.dto.wx.opf;

import cn.cns.wechat.dto.wx.WxApiBack;
import cn.cns.wechat.dto.wx.ext.AuthorizationInfo;
import cn.cns.wechat.dto.wx.ext.AuthorizerInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dhc
 * 2019-05-30 14:01
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxComponentAuthorizer extends WxApiBack {
    @JsonProperty("authorizer_info")
    private AuthorizerInfo authorizerInfo;
    @JsonProperty("authorization_info")
    private AuthorizationInfo authorizationInfo;
}
