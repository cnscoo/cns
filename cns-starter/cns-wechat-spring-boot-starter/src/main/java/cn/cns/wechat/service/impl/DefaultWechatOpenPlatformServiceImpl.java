package cn.cns.wechat.service.impl;

import cn.cns.wechat.dto.wx.WxmpCodeSession;
import cn.cns.wechat.dto.wx.opf.*;
import cn.cns.wechat.dto.wx.opf.params.*;
import cn.cns.wechat.errors.WxApiException;
import cn.cns.wechat.props.WxPlatformProperties;
import cn.cns.wechat.service.WechatOpenPlatformService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;

/**
 * @author dhc
 * 2019-11-24 15:09
 */
@Slf4j
public class DefaultWechatOpenPlatformServiceImpl implements WechatOpenPlatformService {
    /**
     * 获取公众平台 AccessToken 路径
     */
    public static final String API_ACCESS_TOKEN = "%s/cgi-bin/component/api_component_token";
    /**
     * 获取公众平台 PreAuthCode 路径<br>
     * 参数及顺序：<br>
     * - %s: 公众平台的 AccessToken
     */
    public static final String API_PRE_AUTH_CODE = "%s/cgi-bin/component/api_create_preauthcode?component_access_token=%s";
    /**
     * 获取公众号或小程序授权信息<br>
     * 参数及顺序：<br>
     * - %s: 公众平台的 AccessToken
     */
    public static final String API_AUTHORIZATION_INFO = "%s/cgi-bin/component/api_query_auth?component_access_token=%s";
    /**
     * 获取公众号或小程序基本信息<br>
     * 参数及顺序：<br>
     * - %s: 公众平台的 AccessToken
     */
    public static final String API_AUTHORIZER_INFO = "%s/cgi-bin/component/api_get_authorizer_info?component_access_token=%s";
    /**
     * 刷新公众号或小程序AccessToken<br>
     * 参数及顺序：<br>
     * - %s: 公众平台的 AccessToken
     */
    public static final String API_REFRESH_TOKEN = "%s/cgi-bin/component/api_authorizer_token?component_access_token=%s";
    /**
     * 根据小程序的 JS_CODE 获取对应的 Session<br>
     * 参数及顺序：<br>
     * - %s: 公众号或小程序的 AppId<br>
     * - %s: 接收到的 JsCode<br>
     * - %s: 公众平台的 AppId<br>
     * - %s: 公众平台的 AccessToken
     */
    public static final String API_MP_CODE2SESSION = "%s/sns/component/jscode2session?appid=%s&js_code=%s&grant_type=authorization_code&component_appid=%s&component_access_token=%s";

    /**
     * 公众号授权地址（网页扫码打开）<br>
     * 参数及顺序：<br>
     * - %s: 公众平台的 AppId<br>
     * - %s: 公众平台的 PreAuthCode<br>
     * - %s: 公众平台上公众号认证授权后的返回地址 CallbackUrl
     */
    public static final String API_GZH_WEB_AUTH_URL = "https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid=%s&pre_auth_code=%s&redirect_uri=%s";
    /**
     * 公众号授权地址（微信App内打开）<br>
     * 参数及顺序：<br>
     * - %s: 公众平台的 AppId<br>
     * - %s: 公众平台的 PreAuthCode<br>
     * - %s: 公众平台上公众号认证授权后的返回地址 CallbackUrl
     */
    public static final String API_GZH_WX_AUTH_URL = "https://mp.weixin.qq.com/safe/bindcomponent?action=bindcomponent&no_scan=1&component_appid=%s&pre_auth_code=%s&redirect_uri=%s&auth_type=3#wechat_redirect";

    private WxPlatformProperties config;
    private RestTemplate rest;

    public DefaultWechatOpenPlatformServiceImpl(WxPlatformProperties config) {
        this.config = config;
        this.rest = new RestTemplate();
    }

    @Override
    public WxComponentAccessToken getComponentAccessToken(String verifyTicket) throws WxApiException {
        String url = String.format(API_ACCESS_TOKEN, config.getWxApiBaseUrl());
        WxComponentAccessTokenParam param = new WxComponentAccessTokenParam(config.getAppId(), config.getAppSecret(), verifyTicket);
        WxComponentAccessToken back = rest.postForObject(url, param.toJson(), WxComponentAccessToken.class);
        if (back == null || back.hasError()) {
            throw new WxApiException(url, back);
        }
        log.debug("WX >>> 获取微信公众平台 AccessToken 成功，AccessToken = " + back.getToken());
        return back;
    }

    @Override
    public WxComponentPreAuthCode getComponentPreAuthCode(String componentAccessToken) throws WxApiException {
        String url = String.format(API_PRE_AUTH_CODE, config.getWxApiBaseUrl(), componentAccessToken);
        WxApiComponentParam param = new WxApiComponentParam(config.getAppId());
        WxComponentPreAuthCode back = rest.postForObject(url, param.toJson(), WxComponentPreAuthCode.class);
        if (back == null || back.hasError()) {
            throw new WxApiException(url, back);
        }
        log.debug("WX >>> 获取微信公众平台 PreAuthCode 成功，PreAuthCode = " + back.getPreAuthCode());
        return back;
    }

    @Override
    public String getAuthorizationUrl(String preAuthCode, boolean isWeb, String callbackUrl) {
        String api = isWeb ? API_GZH_WEB_AUTH_URL : API_GZH_WX_AUTH_URL;
        return String.format(api, config.getAppId(), preAuthCode, callbackUrl);
    }

    @Override
    public WxComponentRefreshAuthorizerAccessToken refreshAuthorizerAccessToken(String componentAccessToken, String authorizerAppId, String authorizerRefreshToken) throws WxApiException {
        String url = String.format(API_REFRESH_TOKEN, config.getWxApiBaseUrl(), componentAccessToken);
        WxComponentRefreshAuthorizerAccessTokenParam param = new WxComponentRefreshAuthorizerAccessTokenParam(config.getAppId(), authorizerAppId, authorizerRefreshToken);
        WxComponentRefreshAuthorizerAccessToken back = rest.postForObject(url, param.toJson(), WxComponentRefreshAuthorizerAccessToken.class);
        if (back == null || back.hasError()) {
            throw new WxApiException(url, back);
        }
        log.debug("WX >>> 刷新平台授权者授权码成功，AccessToken = " + back.getAuthorizerAccessToken());
        return back;
    }

    @Override
    public WxComponentAuthorization getAuthoirzations(String componentAccessToken, String authorizationCode) throws WxApiException {
        String url = String.format(API_AUTHORIZATION_INFO, config.getWxApiBaseUrl(), componentAccessToken);
        WxComponentAuthorizationParam param = new WxComponentAuthorizationParam(config.getAppId(), authorizationCode);
        WxComponentAuthorization back = rest.postForObject(url, param.toJson(), WxComponentAuthorization.class);
        if (back == null || back.hasError()) {
            throw new WxApiException(url, back);
        }
        log.debug("WX >>> 获取授权者基本信息成功");
        return back;
    }

    @Override
    public WxComponentAuthorizer getAuthorizerInfo(String componentAccessToken, String authorizerAppId) throws WxApiException {
        String url = String.format(API_AUTHORIZER_INFO, config.getWxApiBaseUrl(), componentAccessToken);
        WxComponentAuthorizerParam param = new WxComponentAuthorizerParam(config.getAppId(), authorizerAppId);
        WxComponentAuthorizer back = rest.postForObject(url, param.toJson(), WxComponentAuthorizer.class);
        if (back == null || back.hasError()) {
            throw new WxApiException(url, back);
        }
        log.debug("WX >>> 获取授权者基本信息成功");
        return back;
    }

    @Override
    public WxmpCodeSession wxmpCode2Session(String componentAccessToken, String authorizerAppId, String code) throws WxApiException {
        String url = String.format(API_MP_CODE2SESSION, config.getWxApiBaseUrl(), authorizerAppId, code, config.getAppId(), componentAccessToken);
        WxmpCodeSession back = rest.postForObject(url, null, WxmpCodeSession.class);
        if (back == null || back.hasError()) {
            throw new WxApiException(url, back);
        }
        log.debug("WX >>> 获取小程序登录 Session 成功");
        return back;
    }
}
