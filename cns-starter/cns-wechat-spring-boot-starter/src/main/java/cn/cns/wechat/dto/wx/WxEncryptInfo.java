package cn.cns.wechat.dto.wx;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

/**
 * @author dhc
 * 2019-05-30 11:29
 */
@Data
@JacksonXmlRootElement(localName = "xml")
@JsonIgnoreProperties(ignoreUnknown = true)
public class WxEncryptInfo {
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "AppId")
    private String appId;
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Encrypt")
    private String encrypt;
}
