package cn.cns.wechat.dto.wx.ext;

import cn.cns.wechat.dto.Expires;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author dhc
 * 2019-05-30 14:13
 */
@Data
public class AuthorizationInfo implements Serializable, Expires {
    @JsonProperty("authorizer_appid")
    private String authorizerAppid;
    @JsonProperty("authorizer_access_token")
    private String authorizerAccessToken;
    @JsonProperty("authorizer_refresh_token")
    private String authorizerRefreshToken;
    @JsonProperty("expires_in")
    private Integer expiresIn;
    @JsonProperty("func_info")
    private List<FuncInfo> funcInfos;

    public Set<Integer> getAuthIds() {
        if (this.funcInfos == null) {
            return Collections.emptySet();
        }
        return this.funcInfos.stream()
                .filter(i -> i.getFuncscopeCategory() != null && i.getFuncscopeCategory().getId() != null)
                .map(i -> i.getFuncscopeCategory().getId())
                .collect(Collectors.toSet());
    }

    public String getAuths() {
        return StringUtils.join(this.getAuthIds(), ',');
    }
}
