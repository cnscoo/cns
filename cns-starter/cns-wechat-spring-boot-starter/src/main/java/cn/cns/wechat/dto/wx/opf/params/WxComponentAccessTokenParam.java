package cn.cns.wechat.dto.wx.opf.params;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 获取第三方平台component_access_token的参数
 *
 * @author dhc
 * 2019-05-30 13:23
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxComponentAccessTokenParam extends WxApiComponentParam {
    @JsonProperty("component_appsecret")
    private String componentAppSecret;
    @JsonProperty("component_verify_ticket")
    private String componentVerifyTicket;

    public WxComponentAccessTokenParam() {
    }

    public WxComponentAccessTokenParam(String appId, String appSecret, String verifyTicket) {
        super(appId);
        this.componentAppSecret = appSecret;
        this.componentVerifyTicket = verifyTicket;
    }
}
