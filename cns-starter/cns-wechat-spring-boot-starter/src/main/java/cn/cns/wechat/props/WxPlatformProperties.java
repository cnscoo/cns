package cn.cns.wechat.props;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author dhc
 * 2019-11-21 22:01
 */
@Data
@Accessors(chain = true)
@ConfigurationProperties("cns.wechat.platform")
public class WxPlatformProperties {
    private String wxApiBaseUrl = "https://api.weixin.qq.com";
    private String appId;
    private String appSecret;
    private String token;
    private String encodingAesKey;
    private String eventUrl = "/wx/platform/events";
    private String messageUrl = "/wx/authorizer/{appid}/message";
}
