package cn.cns.wechat.dto.wx.opf;

import cn.cns.wechat.dto.Expires;
import cn.cns.wechat.dto.wx.WxApiBack;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dhc
 * 2019-05-30 18:53
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxComponentPreAuthCode extends WxApiBack implements Expires {
    @JsonProperty("pre_auth_code")
    private String preAuthCode;
    @JsonProperty("expires_in")
    private Integer expiresIn;
}
