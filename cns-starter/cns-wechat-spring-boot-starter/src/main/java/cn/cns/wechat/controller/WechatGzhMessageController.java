package cn.cns.wechat.controller;

import cn.cns.web.utils.BeanCache;
import cn.cns.web.utils.ServletUtils;
import cn.cns.wechat.dto.wx.gzh.WxGzhAesMessage;
import cn.cns.wechat.dto.wx.gzh.WxGzhMessage;
import cn.cns.wechat.props.WxGzhProperties;
import cn.cns.wechat.service.WechatGzhMessageService;
import cn.cns.wechat.utils.WxCrypter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * @author dhc
 * 2019-11-22 21:18
 */
@Slf4j
@ResponseBody
public class WechatGzhMessageController {
    private final WxGzhProperties config;
    private final WechatGzhMessageService service;

    public WechatGzhMessageController(WxGzhProperties config, WechatGzhMessageService service) {
        this.config = config;
        this.service = service;
    }

    /**
     * 校验微信公众号 Token
     *
     * @param signature 微信加密签名
     * @param timestamp 时间戳
     * @param nonce     随机数
     * @param echostr   随机字符串
     * @return 校验结果：成功返回随机字符串，错误返回任意其他字符串
     */
    public String get(@RequestParam String signature, // 微信加密签名
                      @RequestParam String timestamp, // 时间戳
                      @RequestParam String nonce,     // 随机数
                      @RequestParam String echostr) { // 随机字符串
        log.info("WX >>> 微信接口确认: signature={}, timestamp={}, nonce={}, echostr={}", signature, timestamp, nonce, echostr);
        return WxCrypter.sha1(config.getToken(), timestamp, nonce).equals(signature) ? echostr : "Invalid";
    }

    /**
     * 接收微信公众号消息
     *
     * @param request  注入的请求对象
     * @param response 注入的应答对象
     */
    public void post(HttpServletRequest request, HttpServletResponse response) {
        String requestText = ServletUtils.getRequestText(request);
        if (requestText == null) {
            log.info("WX >>> 微信接口空消息内容");
            return;
        }
        XmlMapper mapper = BeanCache.getXmlMapper();
        try {
            log.debug("WX >>> 收到公众号消息:\n" + requestText);
            if (requestText.contains("<Encrypt>")) {
                WxGzhAesMessage message = mapper.readValue(requestText, WxGzhAesMessage.class);
                WxGzhAesMessage backMessage = service.answerAesMessage(message);
                if (backMessage == null) {
                    ServletUtils.responseText(response, "");
                } else {
                    ServletUtils.responseXml(response, backMessage);
                }
            } else if (requestText.contains("<ToUserName>")) {
                WxGzhMessage message = mapper.readValue(requestText, WxGzhMessage.class);
                WxGzhMessage backMessage = service.answerMessage(message);
                if (backMessage == null) {
                    ServletUtils.responseText(response, "");
                } else {
                    ServletUtils.responseXml(response, backMessage);
                }
            } else {
                log.info("WX >>> 未知的微信接口消息：" + requestText);
            }
        } catch (IOException e) {
            log.warn("WX >>> 微信回复消息错误：\n" + requestText, e);
        }
    }
}
