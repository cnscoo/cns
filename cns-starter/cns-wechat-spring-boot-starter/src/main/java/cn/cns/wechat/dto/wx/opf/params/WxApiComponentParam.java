package cn.cns.wechat.dto.wx.opf.params;

import cn.cns.web.utils.BeanCache;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Data;

import java.io.Serializable;

/**
 * @author dhc
 * 2019-05-30 13:29
 */
@Data
public class WxApiComponentParam implements Serializable {
    @JsonProperty("component_appid")
    private String componentAppId;

    public WxApiComponentParam() {
    }

    public WxApiComponentParam(String appId) {
        this.componentAppId = appId;
    }

    public String toJson() {
        try {
            return BeanCache.getJsonMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "";
        }
    }
}
