package cn.cns.wechat;

import cn.cns.web.utils.ContextUtils;
import cn.cns.wechat.controller.WechatGzhMessageController;
import cn.cns.wechat.controller.WechatPlatformController;
import cn.cns.wechat.dto.wx.WxEncryptInfo;
import cn.cns.wechat.dto.wx.gzh.WxGzhAesMessage;
import cn.cns.wechat.itfs.WechatMessageEvent;
import cn.cns.wechat.itfs.WechatOpenPlatformEvent;
import cn.cns.wechat.props.WxGzhProperties;
import cn.cns.wechat.props.WxPlatformProperties;
import cn.cns.wechat.service.WechatGzhMessageService;
import cn.cns.wechat.service.WechatOpenPlatformMessageService;
import cn.cns.wechat.service.impl.DefaultWechatGzhMessageServiceImpl;
import cn.cns.wechat.service.impl.DefaultWechatOpenPlatformMessageServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;

/**
 * @author dhc
 * 2019-11-21 22:03
 */
@Slf4j
@Configuration
@EnableConfigurationProperties({WxGzhProperties.class, WxPlatformProperties.class})
public class WechatStarterAutoConfigure implements InitializingBean {
    public static final String WECHAT_GZH_MESSAGE_CONTROLLER = "wechatGzhMessageController";
    public static final String WECHAT_PLATFORM_CONTROLLER = "WechatPlatformController";

    public WechatStarterAutoConfigure(ConfigurableApplicationContext context) {
        if (ContextUtils.context == null) {
            ContextUtils.context = context;
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("WX >>> 准备检查注册微信监听接口...");
        this.registerWechatControllers();
    }

    private void registerWechatControllers() {
        RequestMappingHandlerMapping mapping = ContextUtils.context.getBean(RequestMappingHandlerMapping.class);

        // 公众号消息接口注册
        WxGzhProperties gzhCfg = ContextUtils.getBean(WxGzhProperties.class);
        if (gzhCfg != null && StringUtils.isNotBlank(gzhCfg.getMessageUrl())) {
            this.registerWxGzhController(mapping, gzhCfg);
        }

        // 公众平台消息接口注册
        WxPlatformProperties platformCfg = ContextUtils.getBean(WxPlatformProperties.class);
        if (platformCfg != null && StringUtils.isNotBlank(platformCfg.getEventUrl())) {
            this.registerWxPlatformController(mapping, platformCfg);
        }
    }

    private void registerWxGzhController(RequestMappingHandlerMapping mapping, WxGzhProperties config) {
        try {
            Assert.notNull(config.getAppId(), "未设置公众号【AppId】");
            Assert.notNull(config.getOriginId(), "未设置公众号【OriginId】");
            Assert.notNull(config.getToken(), "未设置公众号【Token】");
        } catch (Exception e) {
            log.info("WX >>> {}，不注册公众号消息路径", e.getMessage());
            return;
        }

        log.info("WX >>> 微信公众号接口服务配置有效，执行注册消息路径");
        Method get, post;
        try {
            get = WechatGzhMessageController.class.getMethod("get", String.class, String.class, String.class, String.class);
            post = WechatGzhMessageController.class.getMethod("post", HttpServletRequest.class, HttpServletResponse.class);
        } catch (NoSuchMethodException e) {
            log.warn("WX >>> 微信公众号消息路径注册失败", e);
            return;
        }

        WechatGzhMessageService service = ContextUtils.getBean(WechatGzhMessageService.class);
        if (service == null) {
            WechatMessageEvent event = ContextUtils.getBean(WechatMessageEvent.class);
            if (event != null) {
                service = new DefaultWechatGzhMessageServiceImpl(config, event);
            } else {
                log.warn("WX >>> 微信公众号消息服务【{}】未设置，消息路径注册失败", WechatGzhMessageService.class.getSimpleName());
                return;
            }
        }
        // 动态注册公众号消息控制器
        boolean beanRegistered = ContextUtils.registBean(WechatGzhMessageController.class, WechatStarterAutoConfigure.WECHAT_GZH_MESSAGE_CONTROLLER, config, service);
        if (beanRegistered) {
            RequestMappingInfo getInfo = RequestMappingInfo.paths(config.getMessageUrl()).methods(RequestMethod.GET).build();
            mapping.registerMapping(getInfo, WechatStarterAutoConfigure.WECHAT_GZH_MESSAGE_CONTROLLER, get);
            log.info("WX >>> 注册公众号 签名验证 路径：GET:{}", config.getMessageUrl());

            RequestMappingInfo postInfo = RequestMappingInfo.paths(config.getMessageUrl()).methods(RequestMethod.POST).build();
            mapping.registerMapping(postInfo, WechatStarterAutoConfigure.WECHAT_GZH_MESSAGE_CONTROLLER, post);
            log.info("WX >>> 注册公众号 消息接收 路径：POST:{}", config.getMessageUrl());
        }
    }

    private void registerWxPlatformController(RequestMappingHandlerMapping mapping, WxPlatformProperties config) {
        try {
            Assert.notNull(config.getAppId(), "未设置公众平台【AppId】");
            Assert.notNull(config.getAppSecret(), "未设置公众平台【AppSecret】");
            Assert.notNull(config.getToken(), "未设置公众平台【Token】");
        } catch (Exception e) {
            log.info("WX >>> {}，不注册公众平台路径", e.getMessage());
            return;
        }

        log.info("WX >>> 微信公众平台配置有效，执行注册服务路径");
        Method eventMethod, messageMethod;
        try {
            eventMethod = WechatPlatformController.class.getMethod("postEvents", WxEncryptInfo.class);
            messageMethod = WechatPlatformController.class.getMethod("postMessage", String.class, WxGzhAesMessage.class, HttpServletResponse.class);
        } catch (NoSuchMethodException e) {
            log.warn("WX >>> 微信公众平台路服务径注册失败", e);
            return;
        }

        WechatOpenPlatformMessageService service = ContextUtils.getBean(WechatOpenPlatformMessageService.class);
        if (service == null) {
            WechatOpenPlatformEvent event = ContextUtils.getBean(WechatOpenPlatformEvent.class);
            if (event != null) {
                service = new DefaultWechatOpenPlatformMessageServiceImpl(config, event);
            } else {
                log.warn("WX >>> 微信公众平台消息服务【{}】未设置，消息路径注册失败", WechatOpenPlatformMessageService.class.getSimpleName());
                return;
            }
        }
        // 动态注册公众平台事件和消息控制器
        boolean beanRegistered = ContextUtils.registBean(WechatPlatformController.class, WechatStarterAutoConfigure.WECHAT_PLATFORM_CONTROLLER, service);
        if (beanRegistered) {
            RequestMappingInfo eventMapping = RequestMappingInfo.paths(config.getEventUrl()).methods(RequestMethod.POST).build();
            mapping.registerMapping(eventMapping, WechatStarterAutoConfigure.WECHAT_PLATFORM_CONTROLLER, eventMethod);
            log.info("WX >>> 注册公众平台 事件处理 路径：POST:{}", config.getEventUrl());

            RequestMappingInfo messageMapping = RequestMappingInfo.paths(config.getMessageUrl()).methods(RequestMethod.POST).build();
            mapping.registerMapping(messageMapping, WechatStarterAutoConfigure.WECHAT_PLATFORM_CONTROLLER, messageMethod);
            log.info("WX >>> 注册公众平台 消息接收 路径：POST:{}", config.getMessageUrl());
        }
    }
}
