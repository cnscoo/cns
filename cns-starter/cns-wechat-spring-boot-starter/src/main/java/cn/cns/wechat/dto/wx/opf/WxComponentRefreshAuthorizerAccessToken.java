package cn.cns.wechat.dto.wx.opf;

import cn.cns.wechat.dto.Expires;
import cn.cns.wechat.dto.wx.WxApiBack;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dhc
 * 2019-05-30 13:38
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxComponentRefreshAuthorizerAccessToken extends WxApiBack implements Expires {
    @JsonProperty("authorizer_access_token")
    private String authorizerAccessToken;
    @JsonProperty("authorizer_refresh_token")
    private String authorizerRefreshToken;
    @JsonProperty("expires_in")
    private Integer expiresIn;
}
