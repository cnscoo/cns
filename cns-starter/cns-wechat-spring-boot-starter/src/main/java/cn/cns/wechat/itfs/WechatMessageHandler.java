package cn.cns.wechat.itfs;

import cn.cns.wechat.dto.wx.gzh.WxGzhMessage;

/**
 * 微信公众号消息处理器
 *
 * @author dhc
 */
@FunctionalInterface
public interface WechatMessageHandler extends Comparable<WechatMessageHandler> {
    /**
     * 处理消息
     *
     * @param message 收到的消息
     * @return 回应的消息
     */
    WxGzhMessage handle(WxGzhMessage message);

    /**
     * 排序序号
     *
     * @return 序号
     */
    default int order() {
        return 0;
    }

    /**
     * 排序比较
     *
     * @param o 比较对象
     * @return 比较结果值
     */
    @Override
    default int compareTo(WechatMessageHandler o) {
        return this.order() - o.order();
    }
}
