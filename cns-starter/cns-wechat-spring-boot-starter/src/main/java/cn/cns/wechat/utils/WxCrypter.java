package cn.cns.wechat.utils;

import com.qq.weixin.mp.aes.AesException;
import com.qq.weixin.mp.aes.ByteGroup;
import com.qq.weixin.mp.aes.PKCS7Encoder;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;

/**
 * 微信消息加解密工具，参考于官方示例
 *
 * @author dhc
 * 2019-11-21 22:46
 */
public class WxCrypter {
    private static final int BYTE_LEN = 4;

    /**
     * 将字符串组排序合并后使用 SHA1 方式加密
     *
     * @param args 需要加密的字符串组
     * @return 加密后的字符串
     */
    public static String sha1(String... args) {
        Arrays.sort(args);
        String tmpStr = StringUtils.join(args);
        return DigestUtils.sha1Hex(tmpStr);
    }

    /**
     * 生成4个字节的网络字节序
     *
     * @param number 数字
     * @return 生成的字节序
     */
    private static byte[] getNetworkBytesOrder(int number) {
        byte[] bytes = new byte[BYTE_LEN];
        bytes[3] = (byte) (number & 0xFF);
        bytes[2] = (byte) (number >> 8 & 0xFF);
        bytes[1] = (byte) (number >> 16 & 0xFF);
        bytes[0] = (byte) (number >> 24 & 0xFF);
        return bytes;
    }

    /**
     * 还原4个字节的网络字节序
     *
     * @param bytes 需要还原的字节序
     * @return 还原后的数字
     */
    private static int recoverNetworkBytesOrder(byte[] bytes) {
        int number = 0;
        for (int i = 0; i < BYTE_LEN; i++) {
            number <<= 8;
            number |= bytes[i] & 0xff;
        }
        return number;
    }

    /**
     * 加密消息对象字符串为密文内容对象 XML 字符串
     *
     * @param appId          加密的 AppId
     * @param token          加密用的 Token
     * @param encodingAesKey 加密用的 EncodingAesKey
     * @param text           需要加密的消息对象字符串
     * @return 加密消息对象的 XML 符串
     * @throws AesException 加密错误
     */
    public static String encryptToAesMessageXml(String appId, String token, String encodingAesKey, String text) throws AesException {
        String nonce = RandomStringUtils.randomAlphanumeric(16);
        String timeStamp = String.valueOf(Calendar.getInstance().getTimeInMillis() / 1000);
        String encrypt = encrypt(appId, encodingAesKey, nonce, text);
        String msgSignature = sha1(token, timeStamp, nonce, encrypt);

        String xml = "<xml><Encrypt><![CDATA[%s]]></Encrypt><MsgSignature><![CDATA[%s]]></MsgSignature><TimeStamp>%s</TimeStamp><Nonce><![CDATA[%s]]></Nonce></xml>";
        return String.format(xml, encrypt, msgSignature, timeStamp, nonce);
    }

    public static String encrypt(String appId, String encodingAesKey, String randomStr, String text) throws AesException {
        ByteGroup byteCollector = new ByteGroup();
        byte[] randomStrBytes = randomStr.getBytes(StandardCharsets.UTF_8);
        byte[] textBytes = text.getBytes(StandardCharsets.UTF_8);
        byte[] networkBytesOrder = getNetworkBytesOrder(textBytes.length);
        byte[] appidBytes = appId.getBytes(StandardCharsets.UTF_8);

        // randomStr + networkBytesOrder + text + appid
        byteCollector.addBytes(randomStrBytes);
        byteCollector.addBytes(networkBytesOrder);
        byteCollector.addBytes(textBytes);
        byteCollector.addBytes(appidBytes);

        // ... + pad: 使用自定义的填充方式对明文进行补位填充
        byte[] padBytes = PKCS7Encoder.encode(byteCollector.size());
        byteCollector.addBytes(padBytes);

        // 获得最终的字节流, 未加密
        byte[] unencrypted = byteCollector.toBytes();

        try {
            byte[] aesKey = Base64.getDecoder().decode(encodingAesKey);
            // 设置加密模式为AES的CBC模式
            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            SecretKeySpec keySpec = new SecretKeySpec(aesKey, "AES");
            IvParameterSpec iv = new IvParameterSpec(aesKey, 0, 16);
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);

            // 加密
            byte[] encrypted = cipher.doFinal(unencrypted);

            // 使用BASE64对加密后的字符串进行编码
            return Base64.getEncoder().encodeToString(encrypted);
        } catch (Exception e) {
            e.printStackTrace();
            throw new AesException(AesException.ENCRYPT_AES_ERROR);
        }
    }

    /**
     * 解密消息内容
     *
     * @param appid          微信AppId
     * @param encodingAesKey Aes编码Key
     * @param encrypt        加密的字符串
     * @return 解密后的字符串
     * @throws AesException 解密错误
     */
    public static String decrypt(String appid, String encodingAesKey, String encrypt) throws AesException {
        byte[] original;
        try {
            byte[] aesKey = Base64.getDecoder().decode(encodingAesKey);
            // 设置解密模式为AES的CBC模式
            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            SecretKeySpec keySpec = new SecretKeySpec(aesKey, "AES");
            IvParameterSpec iv = new IvParameterSpec(Arrays.copyOfRange(aesKey, 0, 16));
            cipher.init(Cipher.DECRYPT_MODE, keySpec, iv);

            // 使用BASE64对密文进行解码
            byte[] encrypted = Base64.getDecoder().decode(encrypt);

            // 解密
            original = cipher.doFinal(encrypted);
        } catch (Exception e) {
            e.printStackTrace();
            throw new AesException(AesException.DECRYPT_AES_ERROR);
        }

        String xmlContent, fromAppid;
        try {
            // 去除补位字符
            byte[] bytes = PKCS7Encoder.decode(original);

            // 分离16位随机字符串,网络字节序和AppId
            byte[] networkOrder = Arrays.copyOfRange(bytes, 16, 20);

            int xmlLength = recoverNetworkBytesOrder(networkOrder);

            xmlContent = new String(Arrays.copyOfRange(bytes, 20, 20 + xmlLength), StandardCharsets.UTF_8);
            fromAppid = new String(Arrays.copyOfRange(bytes, 20 + xmlLength, bytes.length), StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
            throw new AesException(AesException.ILLEGAL_BUFFER);
        }

        // appid不相同的情况
        if (!fromAppid.equals(appid)) {
            throw new AesException(AesException.VALIDATE_APPID_ERROR);
        }
        return xmlContent;
    }
}
