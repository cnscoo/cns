package cn.cns.wechat.service;

import cn.cns.wechat.dto.wx.WxEncryptInfo;
import cn.cns.wechat.dto.wx.gzh.WxGzhAesMessage;

/**
 * @author dhc
 * 2019-11-21 22:06
 */
public interface WechatOpenPlatformMessageService {
    /**
     * 处理微信公众平台推送的事件
     *
     * @param encryptInfo 事件内容
     */
    void onComponentTicketEvent(WxEncryptInfo encryptInfo);

    /**
     * 处理微信公众平台推送的公众消息
     *
     * @param appid      公众号AppId
     * @param aesMessage 公众号消息内容
     * @return 应答消息内容
     */
    WxGzhAesMessage onComponentGzhAesMessage(String appid, WxGzhAesMessage aesMessage);
}
