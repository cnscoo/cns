package cn.cns.wechat.dto.wx.opf.params;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 换取公众号或小程序的接口调用凭据和授权信息的参数
 *
 * @author dhc
 * 2019-05-30 13:33
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxComponentAuthorizationParam extends WxApiComponentParam {
    @JsonProperty("authorization_code")
    private String authorizationCode;

    public WxComponentAuthorizationParam(String appId, String authorizationCode) {
        super(appId);
        this.authorizationCode = authorizationCode;
    }
}
