package cn.cns.wechat.service;

import cn.cns.wechat.dto.wx.WxmpCodeSession;
import cn.cns.wechat.dto.wx.opf.*;
import cn.cns.wechat.errors.WxApiException;

/**
 * @author dhc
 * 2019-11-24 14:37
 */
public interface WechatOpenPlatformService {

    /**
     * 获取平台AccessToken
     *
     * @param verifyTicket 微信定时推送的 VerifyTicket
     * @return 平台 AccessToken 对象
     * @throws WxApiException 获取出错
     */
    WxComponentAccessToken getComponentAccessToken(String verifyTicket) throws WxApiException;

    /**
     * 获取平台页面授期的 PreAuthCode
     *
     * @param componentAccessToken 平台的 AccessToken
     * @return 平台 PreAuthCode 对象
     * @throws WxApiException 获取出错
     */
    WxComponentPreAuthCode getComponentPreAuthCode(String componentAccessToken) throws WxApiException;

    /**
     * 获取平台的微信页面授权地址
     *
     * @param preAuthCode 平台授权的 PreAuthCode
     * @param isWeb       是否Web访问（或者Mobile）
     * @param callbackUrl 授权返回地址
     * @return 授权地址
     */
    String getAuthorizationUrl(String preAuthCode, boolean isWeb, String callbackUrl);

    /**
     * 刷新公众号或小程序的 AccessToken
     *
     * @param componentAccessToken   平台的 AccessToken
     * @param authorizerAppId        公众号或小程序的 AppId
     * @param authorizerRefreshToken 公众号或小程序的 RefreshToken
     * @return 刷新 AccessToken 的返回对象
     * @throws WxApiException 刷新出错
     */
    WxComponentRefreshAuthorizerAccessToken refreshAuthorizerAccessToken(String componentAccessToken, String authorizerAppId, String authorizerRefreshToken) throws WxApiException;

    /**
     * 获取公众号或小程序对平台的授权信息
     *
     * @param componentAccessToken 平台 AccessToken
     * @param authorizationCode    授权码
     * @return 授权信息
     * @throws WxApiException 获取出错
     */
    WxComponentAuthorization getAuthoirzations(String componentAccessToken, String authorizationCode) throws WxApiException;

    /**
     * 获取公众号或小程序的基本信息
     *
     * @param componentAccessToken 平台的 AccessToken
     * @param authorizerAppId      公众号或小程序的 AppId
     * @return 公众号或小程序的基本信息
     * @throws WxApiException 获取出错
     */
    WxComponentAuthorizer getAuthorizerInfo(String componentAccessToken, String authorizerAppId) throws WxApiException;

    /**
     * 获取小程序用户登录 SessionKey
     *
     * @param componentAccessToken 平台的 AccessToken
     * @param authorizerAppId      小程序的 AppId
     * @param code                 小程序登录时的 Code
     * @return 小程序登录的 Session
     * @throws WxApiException 微信接口调用错误
     */
    WxmpCodeSession wxmpCode2Session(String componentAccessToken, String authorizerAppId, String code) throws WxApiException;
}
