package cn.cns.wechat.dto.wx.ext;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author dhc
 * 2019-05-30 14:11
 */
@Data
public class AuthorizerInfo implements Serializable {
    private static final List<String> BUSINESSES = new ArrayList<>() {{
        add("open_store");
        add("open_scan");
        add("open_pay");
        add("open_card");
        add("open_shake");
    }};

    @JsonProperty("nick_name")
    private String nickName;
    @JsonProperty("head_img")
    private String headImg;
    @JsonProperty("service_type_info")
    private FuncInfo.FuncId serviceTypeInfo;
    @JsonProperty("verify_type_info")
    private FuncInfo.FuncId verifyTypeInfo;
    @JsonProperty("user_name")
    private String userName;
    @JsonProperty("principal_name")
    private String principalName;
    @JsonProperty("business_info")
    private Map<String, Byte> businessInfo;
    private String alias;
    @JsonProperty("qrcode_url")
    private String qrcodeUrl;
    private String signature;
    @JsonProperty("MiniProgramInfo")
    private Map<String, Object> miniProgramInfo;

    /**
     * 获取开通功能的值：门店(store)、扫商品(scan)、支付(pay)、卡券(card)、摇一摇(shake）
     *
     * @return 二进制合并后的数字
     */
    public int getBusiness() {
        int value = 0;
        if (this.businessInfo == null) {
            return value;
        }
        for (int i = 0; i < BUSINESSES.size(); i++) {
            String biz = BUSINESSES.get(i);
            if (this.businessInfo.get(biz) == 1) {
                value = value | (1 << i);
            }
        }
        return value;
    }
}
