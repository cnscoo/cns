package cn.cns.wechat.errors;

import cn.cns.wechat.dto.wx.WxApiBack;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dhc
 * 2019-11-24 15:50
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxApiException extends Exception {
    private String url;
    private Integer errCode;
    private String errMsg;

    public WxApiException(String url, Integer errCode, String errMsg) {
        super(errMsg);
        this.url = url;
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    public WxApiException(String url, WxApiBack back) {
        super(back == null ? "返回数据为空" : back.getErrMsg());
        this.url = url;
        if (back != null) {
            this.errCode = back.getErrCode();
            this.errMsg = back.getErrMsg();
        } else {
            this.errMsg = super.getMessage();
        }
    }
}
