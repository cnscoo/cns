package cn.cns.wechat.props;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author dhc
 * 2019-12-18 23:02
 */
@Data
@Accessors(chain = true)
@ConfigurationProperties("cns.wechat.gzh")
public class WxGzhProperties {
    private String originId;
    private String appId;
    private String appSecret;
    private String token;
    private String encodingAesKey;
    private String messageUrl = "/wx/api";
}
