package cn.cns.wechat.dto.wx;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 微信小程序用Code换区Session返回的对象
 *
 * @author dhc
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxmpCodeSession extends WxApiBack {
    private String openid;
    @JsonProperty("session_key")
    private String sessionKey;
    private String unionid;
}
