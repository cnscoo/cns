package cn.cns.wechat.service.impl;

import cn.cns.web.utils.BeanCache;
import cn.cns.wechat.dto.wx.WxEncryptInfo;
import cn.cns.wechat.dto.wx.gzh.WxGzhAesMessage;
import cn.cns.wechat.dto.wx.opf.WxComponentEvent;
import cn.cns.wechat.itfs.WechatOpenPlatformEvent;
import cn.cns.wechat.props.WxPlatformProperties;
import cn.cns.wechat.service.WechatOpenPlatformMessageService;
import cn.cns.wechat.utils.WxCrypter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.qq.weixin.mp.aes.AesException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * @author dhc
 * 2019-11-21 23:04
 */
@Slf4j
public class DefaultWechatOpenPlatformMessageServiceImpl extends BaseWechatGzhMessageServiceImpl implements WechatOpenPlatformMessageService {
    private WxPlatformProperties config;
    private WechatOpenPlatformEvent event;

    public DefaultWechatOpenPlatformMessageServiceImpl(WxPlatformProperties config, WechatOpenPlatformEvent event) {
        super(event);
        this.config = config;
        this.event = event;
    }

    @Override
    public void onComponentTicketEvent(WxEncryptInfo encryptInfo) {
        log.debug("WX >>> 收到微信推送到平台的消息：\nAppId = {}, Encrypt = {}", encryptInfo.getAppId(), encryptInfo.getEncrypt());

        String decrypt;
        try {
            decrypt = WxCrypter.decrypt(config.getAppId(), config.getEncodingAesKey(), encryptInfo.getEncrypt());
        } catch (AesException e) {
            log.error("WX >>> 解密微信公众平台消息失败，Encrypt = " + encryptInfo.getEncrypt(), e);
            return;
        }
        log.debug("WX >>> 解密微信推送的 ComponentVerifyTicket:\n{}", decrypt);

        WxComponentEvent ticket;
        try {
            ticket = BeanCache.getXmlMapper().readValue(decrypt, WxComponentEvent.class);
        } catch (JsonProcessingException e) {
            log.error("WX >>> XML反序列化微信推送到平台的消息失败，XML:\n" + decrypt, e);
            return;
        }
        event.onEvent(ticket);
    }

    @Override
    public WxGzhAesMessage onComponentGzhAesMessage(String appid, WxGzhAesMessage aesMessage) {
        if (StringUtils.isBlank(config.getEncodingAesKey())) {
            log.warn("WX >>> 未设置公众平台 encodingAesKey，无法解密公众号消息");
            return event.onUnSetGzhAesMessage(aesMessage);
        }
        return super.answerAesMessage(config.getAppId(), config.getEncodingAesKey(), config.getToken(), aesMessage);
    }
}
