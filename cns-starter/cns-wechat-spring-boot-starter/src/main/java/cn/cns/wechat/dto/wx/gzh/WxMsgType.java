package cn.cns.wechat.dto.wx.gzh;

import java.util.Arrays;
import java.util.Optional;

/**
 * 微信消息类型
 *
 * @author dhc
 * 2016-07-05 10:12
 */
public enum WxMsgType {
    /**
     * 文本消息
     */
    TEXT("text", true, true),
    /**
     * 图片消息
     */
    IMAGE("image", true, true),
    /**
     * 语音消息
     */
    VOICE("voice", true, true),
    /**
     * 视频消息
     */
    VIDEO("video", true, true),
    /**
     * 小视频消息（不可回复类型）
     */
    SHORTVIDEO("shortvideo", true, false),
    /**
     * 位置消息，带有坐标信息（不可回复类型）
     */
    LOCATION("location", true, false),
    /**
     * 链接消息（不可回复类型）
     */
    LINK("link", true, false),
    /**
     * 事件消息（不可回复类型）
     */
    EVENT("event", true, false),
    /**
     * 应答音乐消息（不可接收类型）
     */
    MUSIC("music", false, true),
    /**
     * 文章（图文）消息（不可接收类型）
     */
    NEWS("news", false, true);

    /**
     * 类型代码
     */
    public final String code;
    /**
     * 是否可以接收到该类型消息
     */
    public final boolean receive;
    /**
     * 是否能够应答该类型消息
     */
    public final boolean answer;

    WxMsgType(String code, boolean receive, boolean answer) {
        this.code = code;
        this.receive = receive;
        this.answer = answer;
    }

    public static Optional<WxMsgType> of(String code) {
        return Arrays.stream(values()).filter(t -> t.code.equals(code)).findFirst();
    }
}
