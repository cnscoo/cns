package cn.cns.wechat.service;

import cn.cns.wechat.dto.wx.gzh.WxGzhAesMessage;
import cn.cns.wechat.dto.wx.gzh.WxGzhMessage;

/**
 * @author dhc
 * 2019-11-22 21:19
 */
public interface WechatGzhMessageService {
    /**
     * 处理微信加密消息
     *
     * @param message 加密消息的内容
     * @return 加密的应答消息
     */
    WxGzhAesMessage answerAesMessage(WxGzhAesMessage message);

    /**
     * 处理微信消息
     *
     * @param message 消息对象
     * @return 应答消息对象
     */
    WxGzhMessage answerMessage(WxGzhMessage message);
}
