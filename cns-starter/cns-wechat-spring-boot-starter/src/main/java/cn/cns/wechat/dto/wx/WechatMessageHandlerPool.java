package cn.cns.wechat.dto.wx;

import cn.cns.wechat.dto.wx.gzh.WxGzhMessage;
import cn.cns.wechat.dto.wx.gzh.WxMsgType;
import cn.cns.wechat.itfs.WechatMessageHandler;

import java.util.*;

/**
 * 消息处理池
 */
public class WechatMessageHandlerPool {
    private static final Map<WxMsgType, List<WechatMessageHandler>> HANDLER_MAP = new HashMap<>();

    /**
     * 添加消息处理器
     *
     * @param type    消息类型
     * @param handler 处理器
     */
    public static synchronized void addHandler(WxMsgType type, WechatMessageHandler handler) {
        if (type == null || handler == null) {
            return;
        }
        List<WechatMessageHandler> handlers = HANDLER_MAP.computeIfAbsent(type, t -> new ArrayList<>());
        if (!handlers.contains(handler)) {
            handlers.add(handler);
            if (handlers.size() > 1) {
                handlers.sort(Comparable::compareTo);
            }
        }
    }

    /**
     * 删除消息处理器
     *
     * @param type    消息类型
     * @param handler 处理器
     * @return 是否成功删除
     */
    public static boolean delHandler(WxMsgType type, WechatMessageHandler handler) {
        if (type == null || handler == null) {
            return false;
        }
        return Optional.ofNullable(HANDLER_MAP.get(type)).map(i -> i.remove(handler)).orElse(false);
    }

    /**
     * 处理消息
     *
     * @param type    消息类型
     * @param message 微信消息
     * @return 处理后返回的消息
     */
    public static WxGzhMessage handle(WxMsgType type, WxGzhMessage message) {
        if (type == null || message == null) {
            return null;
        }
        List<WechatMessageHandler> handlers = HANDLER_MAP.get(type);
        if (handlers == null || handlers.isEmpty()) {
            return null;
        }
        for (WechatMessageHandler handler : handlers) {
            WxGzhMessage back;
            if ((back = handler.handle(message)) != null) {
                return back;
            }
        }
        return null;
    }
}
