package cn.cns.wechat.dto.wx.ext;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author dhc
 * 2019-05-30 14:03
 */
@Data
public class FuncInfo implements Serializable {
    @JsonProperty("funcscope_category")
    private FuncId funcscopeCategory;

    public static class FuncId implements Serializable {
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }
}
