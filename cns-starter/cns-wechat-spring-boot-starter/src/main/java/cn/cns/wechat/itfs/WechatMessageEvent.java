package cn.cns.wechat.itfs;

import cn.cns.wechat.dto.wx.WechatMessageHandlerPool;
import cn.cns.wechat.dto.wx.gzh.WxGzhAesMessage;
import cn.cns.wechat.dto.wx.gzh.WxGzhMessage;
import cn.cns.wechat.dto.wx.gzh.WxMsgType;
import com.qq.weixin.mp.aes.AesException;

import static cn.cns.wechat.dto.wx.gzh.WxMsgType.TEXT;

/**
 * @author dhc
 * 2019-11-22 22:04
 */
public interface WechatMessageEvent {

    /**
     * 解密微信公众加密消息内容失败
     *
     * @param aesMessage 加密的消息内容
     * @param e          错误对象
     * @return 回答的消息内容
     */
    default WxGzhMessage onDecodeAesMessageException(WxGzhAesMessage aesMessage, AesException e) {
        return null;
    }

    /**
     * 处理未配置的公众号的加密消息
     *
     * @param aesMessage 加密的消息内容
     * @return 回答的加密内容
     */
    default WxGzhAesMessage onUnSetGzhAesMessage(WxGzhAesMessage aesMessage) {
        return null;
    }

    /**
     * 当接收到无法识别的消息类型时
     *
     * @param message 未知类型的消息
     * @return 回答的消息内容
     */
    default WxGzhMessage onUnknownTypeMessage(WxGzhMessage message) {
        return null;
    }

    /**
     * 当接收到公众号消息时
     *
     * @param message 公众号消息内容
     * @return 回复消息内容
     */
    default WxGzhMessage onMessage(WxGzhMessage message) {
        WxMsgType type = WxMsgType.of(message.getMsgType()).orElse(null);
        if (type == null) {
            return this.onUnknownTypeMessage(message);
        } else {
            WxGzhMessage back = WechatMessageHandlerPool.handle(type, message);
            if (back != null) {
                return back;
            }
            switch (type) {
                case TEXT:
                    return this.onTextMessage(message);
                case IMAGE:
                    return this.onImageMessage(message);
                case VOICE:
                    return this.onVoiceMessage(message);
                case VIDEO:
                    return this.onVideoMessage(message);
                case SHORTVIDEO:
                    return this.onShortVideoMessage(message);
                case LOCATION:
                    return this.onLocationMessage(message);
                case LINK:
                    return this.onLinkMessage(message);
                case EVENT:
                    return this.onEventMessage(message);
                default:
                    back = new WxGzhMessage();
                    back.setMsgType(TEXT.code);
                    back.setContent("success");
                    return back;
            }
        }
    }

    /**
     * 当接收到文本消息时
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onTextMessage(WxGzhMessage message) {
        return null;
    }

    /**
     * 当接收到图片消息时
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onImageMessage(WxGzhMessage message) {
        return null;
    }

    /**
     * 当接收到语音消息时
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onVoiceMessage(WxGzhMessage message) {
        return null;
    }

    /**
     * 当接收到视频消息时
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onVideoMessage(WxGzhMessage message) {
        return null;
    }

    /**
     * 当接收到短视频消息时
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onShortVideoMessage(WxGzhMessage message) {
        return null;
    }

    /**
     * 当接收到位置消息时
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onLocationMessage(WxGzhMessage message) {
        return null;
    }

    /**
     * 当接收到链接消息时
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onLinkMessage(WxGzhMessage message) {
        return null;
    }

    /**
     * 当接收到事件消息时
     *
     * @param message 事件消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onEventMessage(WxGzhMessage message) {
        switch (message.getEvent()) {
            case "subscribe":
                return this.onSubscribeEvent(message);
            case "unsubscribe":
                return this.onUnSubscribeEvent(message);
            case "scancode_push":
                return this.onScancodePushEvent(message);
            case "scancode_waitmsg":
                return this.onScancodeWaitmsgEvent(message);
            case "pic_sysphoto":
                return this.onPicSysphotoEvent(message);
            case "pic_photo_or_album":
                return this.onPicPhotoOrAlbumEvent(message);
            case "pic_weixin":
                return this.onPicWeixinEvent(message);
            case "location_select":
                return this.onLocationSelectEvent(message);
            case "view_miniprogram":
                return this.onViewMiniProgramEvent(message);
            case "LOCATION":
                return this.onLocationEvent(message);
            case "CLICK":
                return this.onClickEvent(message);
            case "VIEW":
                return this.onViewEvent(message);
            default:
                return null;
        }
    }

    /**
     * 当有用户关注时
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onSubscribeEvent(WxGzhMessage message) {
        return null;
    }

    /**
     * 当有用户取消关注时
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onUnSubscribeEvent(WxGzhMessage message) {
        return null;
    }

    /**
     * 当有用户扫码时
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onScancodePushEvent(WxGzhMessage message) {
        return null;
    }

    /**
     * 当有用户扫码且弹出"消息接收中"提示框时
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onScancodeWaitmsgEvent(WxGzhMessage message) {
        return null;
    }

    /**
     * 当有用户点击菜单弹出系统拍照发图
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onPicSysphotoEvent(WxGzhMessage message) {
        return null;
    }

    /**
     * 当有用户点击菜单弹出拍照或者相册发图
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onPicPhotoOrAlbumEvent(WxGzhMessage message) {
        return null;
    }

    /**
     * 当有用户点击菜单弹出微信相册发图器
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onPicWeixinEvent(WxGzhMessage message) {
        return null;
    }

    /**
     * 当有用户点击菜单弹出地理位置选择器
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onLocationSelectEvent(WxGzhMessage message) {
        return null;
    }

    /**
     * 当有用户点击菜单跳转小程序
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onViewMiniProgramEvent(WxGzhMessage message) {
        return null;
    }

    /**
     * 用户上报位置
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onLocationEvent(WxGzhMessage message) {
        return null;
    }

    /**
     * 用户点击菜单
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onClickEvent(WxGzhMessage message) {
        return null;
    }

    /**
     * 用户点击菜单跳转链接时
     *
     * @param message 消息内容
     * @return 回答的消息内容
     */
    default WxGzhMessage onViewEvent(WxGzhMessage message) {
        return null;
    }
}
