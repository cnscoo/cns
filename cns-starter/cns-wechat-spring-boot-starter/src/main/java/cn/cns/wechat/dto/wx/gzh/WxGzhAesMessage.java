package cn.cns.wechat.dto.wx.gzh;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

/**
 * @author dhc
 * 2018-01-31 15:01
 */
@Data
@JacksonXmlRootElement(localName = "xml")
@JsonIgnoreProperties(ignoreUnknown = true)
public class WxGzhAesMessage {
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "ToUserName")
    private String toUserName;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Encrypt")
    private String encrypt;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "MsgSignature")
    private String msgSignature;

    @JacksonXmlProperty(localName = "TimeStamp")
    private String timeStamp;

    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Nonce")
    private String nonce;
}
