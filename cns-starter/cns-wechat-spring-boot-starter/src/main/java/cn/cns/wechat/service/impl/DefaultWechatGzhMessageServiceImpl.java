package cn.cns.wechat.service.impl;

import cn.cns.wechat.dto.wx.gzh.WxGzhAesMessage;
import cn.cns.wechat.itfs.WechatMessageEvent;
import cn.cns.wechat.props.WxGzhProperties;
import cn.cns.wechat.service.WechatGzhMessageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * @author dhc
 * 2019-11-22 21:20
 */
@Slf4j
public class DefaultWechatGzhMessageServiceImpl extends BaseWechatGzhMessageServiceImpl implements WechatGzhMessageService {
    private WxGzhProperties config;

    public DefaultWechatGzhMessageServiceImpl(WxGzhProperties config, WechatMessageEvent event) {
        super(event);
        this.config = config;
    }

    @Override
    public WxGzhAesMessage answerAesMessage(WxGzhAesMessage aesMessage) {
        if (!aesMessage.getToUserName().equals(config.getOriginId())) {
            return event.onUnSetGzhAesMessage(aesMessage);
        }

        if (StringUtils.isBlank(config.getEncodingAesKey())) {
            log.warn("WX >>> 未设置公众号 encodingAesKey，无法解密公众号消息");
            return event.onUnSetGzhAesMessage(aesMessage);
        }

        return super.answerAesMessage(config.getAppId(), config.getEncodingAesKey(), config.getToken(), aesMessage);
    }
}
