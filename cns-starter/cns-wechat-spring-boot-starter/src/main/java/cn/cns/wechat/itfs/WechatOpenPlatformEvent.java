package cn.cns.wechat.itfs;

import cn.cns.wechat.dto.wx.opf.WxComponentEvent;

import java.time.LocalDateTime;

/**
 * @author dhc
 * 2019-11-24 14:22
 */
public interface WechatOpenPlatformEvent extends WechatMessageEvent {

    /**
     * 当接收到微信开放平台推送的事件时
     *
     * @param e 事件内容
     */
    default void onEvent(WxComponentEvent e) {
        switch (e.getInfoType()) {
            case "component_verify_ticket":
                this.onComponentVerifyTicket(e.getAppId(), e.getComponentVerifyTicket(), e.getCreateDateTime());
                break;
            case "authorized":
                this.onAuthorized(e.getAppId(), e.getAuthorizerAppId(), e.getAuthorizationCode(), e.getAuthorizationCodeExpiredDateTime(), e.getPreAuthCode(), e.getCreateDateTime());
                break;
            case "updateauthorized":
                this.onUpdateauthorized(e.getAppId(), e.getAuthorizerAppId(), e.getAuthorizationCode(), e.getAuthorizationCodeExpiredDateTime(), e.getPreAuthCode(), e.getCreateDateTime());
                break;
            case "unauthorized":
                this.onUnAuthorized(e.getAppId(), e.getAuthorizerAppId(), e.getCreateDateTime());
                break;
            case "notify_third_fasteregister":
                this.onNotifyThirdFasteregister(e.getAppId(), e.getMpAppId(), e.getStatus(), e.getAuthCode(), e.getInfo(), e.getCreateDateTime());
                break;
            default:
        }
    }

    /**
     * 当接收到微信推送的定时验证消息时
     *
     * @param appId          平台AppId
     * @param verifyTicket   验证票码
     * @param createDateTime 推送时间
     */
    void onComponentVerifyTicket(String appId, String verifyTicket, LocalDateTime createDateTime);

    /**
     * 当有公众号完成授权验证时
     *
     * @param appId                            平台AppId
     * @param authorizerAppId                  公众号或小程序的 appid
     * @param authorizationCode                授权码，可用于获取授权信息
     * @param authorizationCodeExpiredDateTime 授权码过期时间
     * @param preAuthCode                      预授权码
     * @param createDateTime                   授权时间
     */
    void onAuthorized(String appId, String authorizerAppId, String authorizationCode, LocalDateTime authorizationCodeExpiredDateTime, String preAuthCode, LocalDateTime createDateTime);

    /**
     * 当有公众号更新授权验证时
     *
     * @param appId                            平台AppId
     * @param authorizerAppId                  公众号或小程序的 appid
     * @param authorizationCode                授权码，可用于获取授权信息
     * @param authorizationCodeExpiredDateTime 授权码过期时间
     * @param preAuthCode                      预授权码
     * @param createDateTime                   更新授权时间
     */
    void onUpdateauthorized(String appId, String authorizerAppId, String authorizationCode, LocalDateTime authorizationCodeExpiredDateTime, String preAuthCode, LocalDateTime createDateTime);

    /**
     * 当有公众号取消授权验证后
     *
     * @param appId           平台AppId
     * @param authorizerAppId 公众号或小程序的 appid
     * @param createDateTime  取消授权时间
     */
    void onUnAuthorized(String appId, String authorizerAppId, LocalDateTime createDateTime);

    /**
     * 当有小程序请求注册验证时
     *
     * @param appId          平台AppId
     * @param mpAppId        创建小程序AppId
     * @param status         状态码，参见：https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/Mini_Programs/Fast_Registration_Interface_document.html
     * @param authCode       XXX第三方授权码
     * @param info           注册信息
     * @param createDateTime 请求时间
     */
    void onNotifyThirdFasteregister(String appId, String mpAppId, int status, String authCode, WxComponentEvent.Info info, LocalDateTime createDateTime);

}
