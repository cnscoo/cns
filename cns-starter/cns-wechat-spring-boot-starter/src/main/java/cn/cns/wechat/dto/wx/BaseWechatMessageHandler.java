package cn.cns.wechat.dto.wx;

import cn.cns.wechat.itfs.WechatMessageHandler;

/**
 * 默认的消息处理器
 *
 * @author dhc
 */
public abstract class BaseWechatMessageHandler implements WechatMessageHandler {

    @Override
    public int compareTo(WechatMessageHandler o) {
        return this.order() - o.order();
    }
}
