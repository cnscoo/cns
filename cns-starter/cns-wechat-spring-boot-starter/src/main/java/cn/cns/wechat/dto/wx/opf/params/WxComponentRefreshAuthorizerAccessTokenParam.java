package cn.cns.wechat.dto.wx.opf.params;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 获取（刷新）授权公众号或小程序的接口调用凭据（令牌）的参数
 *
 * @author dhc
 * 2019-05-30 13:35
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxComponentRefreshAuthorizerAccessTokenParam extends WxApiComponentParam {
    @JsonProperty("authorizer_appid")
    private String authorizerAppId;
    @JsonProperty("authorizer_refresh_token")
    private String authorizerRefreshToken;

    public WxComponentRefreshAuthorizerAccessTokenParam(String appId, String authorizerAppId, String authorizerRefreshToken) {
        super(appId);
        this.authorizerAppId = authorizerAppId;
        this.authorizerRefreshToken = authorizerRefreshToken;
    }
}
