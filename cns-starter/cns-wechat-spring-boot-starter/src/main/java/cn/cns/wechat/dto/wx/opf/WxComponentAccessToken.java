package cn.cns.wechat.dto.wx.opf;

import cn.cns.wechat.dto.Expires;
import cn.cns.wechat.dto.wx.WxApiBack;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dhc
 * 2019-05-30 13:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxComponentAccessToken extends WxApiBack implements Expires {
    @JsonProperty("component_access_token")
    private String token;
    @JsonProperty("expires_in")
    private Integer expiresIn;
}
