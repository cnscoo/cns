package cn.cns.socket.ws.enums;

import org.springframework.lang.NonNull;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public interface WsBizType {
    Map<Integer, WsBizType> VALUES = new ConcurrentHashMap<>();

    static <T extends WsBizType> void load(@NonNull Class<T> clz) {
        if (clz.isEnum()) {
            for (WsBizType bizType : clz.getEnumConstants()) {
                VALUES.putIfAbsent(bizType.getCode(), bizType);
            }
        }
    }

    static <T extends WsBizType> void unload(@NonNull Class<T> clz) {
        if (clz.isEnum()) {
            System.out.println("WS >>> 卸载 BizType: " + clz.getSimpleName());
            for (WsBizType bizType : clz.getEnumConstants()) {
                VALUES.remove(bizType.getCode());
            }
        }
    }

    int getCode();

    String getName();
}
