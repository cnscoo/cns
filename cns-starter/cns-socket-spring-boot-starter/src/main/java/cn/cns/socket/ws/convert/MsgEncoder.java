package cn.cns.socket.ws.convert;

import cn.cns.socket.ws.WsMessage;
import cn.cns.web.utils.BeanCache;
import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.websocket.EncodeException;
import jakarta.websocket.Encoder;
import jakarta.websocket.EndpointConfig;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class MsgEncoder implements Encoder.Binary<WsMessage> {

    @Override
    public ByteBuffer encode(WsMessage message) throws EncodeException {
        if (message.getDataType() == null) {
            return ByteBuffer.allocate(0);
        }
        int type = message.getDataType().getCode();
        byte[] bytes = null;
        if (message.getData() != null) {
            switch (message.getDataType()) {
                case PING:
                case PONG:
                    break;
                case TEXT:
                    String str = (message.getData() instanceof String) ? (String) message.getData() : message.getData().toString();
                    bytes = str.getBytes(StandardCharsets.UTF_8);
                    break;
                case JSON:
                    try {
                        bytes = BeanCache.getJsonMapper().writeValueAsBytes(message.getData());
                    } catch (JsonProcessingException e) {
                        throw new EncodeException(message, "序列化 data 为 JSON 出错", e);
                    }
                    break;
                case XML:
                    try {
                        bytes = BeanCache.getXmlMapper().writeValueAsBytes(message.getData());
                    } catch (JsonProcessingException e) {
                        throw new EncodeException(message, "序列化 data 为 XML 出错", e);
                    }
                    break;
                default:
                    bytes = "暂不支持的数据类型".getBytes(StandardCharsets.UTF_8);
                    break;
            }
        }
        ByteBuffer buffer;
        if (bytes == null) {
            buffer = ByteBuffer.allocate(8);
            buffer.putInt(message.getBizType().getCode());
            buffer.putInt(type);
        } else {
            int len = bytes.length;
            buffer = ByteBuffer.allocate(12 + len);
            buffer.putInt(message.getBizType().getCode());
            buffer.putInt(type);
            buffer.putInt(len);
            buffer.put(bytes);
        }
        buffer.position(0);
        return buffer;
    }

    @Override
    public void init(EndpointConfig config) {

    }

    @Override
    public void destroy() {

    }
}
