package cn.cns.socket.ws;

import cn.cns.socket.ws.enums.WsBizType;

import jakarta.websocket.CloseReason;
import jakarta.websocket.Session;
import java.util.function.BiConsumer;

@FunctionalInterface
public interface WsEventHandler extends Comparable<WsEventHandler> {

    static WsEventHandler newBizMessageHandler(WsBizType bizType, BiConsumer<Session, WsMessage> messageConsumer) {
        return new WsEventHandler() {
            @Override
            public WsBizType getBizType() {
                return bizType;
            }

            @Override
            public void onMessage(Session session, WsMessage message) {
                messageConsumer.accept(session, message);
            }
        };
    }

    default WsBizType getBizType() {
        return null;
    }

    default void onOpen(Session session) {

    }

    default void onClose(Session session, CloseReason reason) {

    }

    default void onError(Session session, Throwable thr) {

    }

    void onMessage(Session session, WsMessage message);

    default boolean supportSession(Session session) {
        return true;
    }

    default boolean supportMessage(Session session, WsMessage message) {
        WsBizType bizType = getBizType();
        return bizType == null || bizType.equals(message.getBizType());
    }

    default int getOrder() {
        return 0;
    }

    @Override
    default int compareTo(WsEventHandler o) {
        return this.getOrder() - o.getOrder();
    }
}
