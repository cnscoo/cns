package cn.cns.socket.ws.enums;

public enum WsBizBaseType implements WsBizType {
    DETECT(0, "探测"),
    OTHER(1, "其他"),
    API(101, "接口"),
    IM(201, "聊天"),
    ;

    private final int code;
    private final String name;

    WsBizBaseType(int code, String name) {
        this.code = code;
        this.name = name;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getName() {
        return name;
    }
}
