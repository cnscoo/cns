package cn.cns.socket.ws.enums;

import java.util.Arrays;

public enum WsDataType {
    PING(1),
    PONG(2),
    TEXT(100),
    JSON(101),
    XML(102),
    BYTES(200),
    IMAGE(201),
    ;

    private final int code;

    WsDataType(int code) {
        this.code = code;
    }

    public static WsDataType of(int code) {
        return Arrays.stream(values()).filter(i -> i.code == code).findFirst().orElse(null);
    }

    public boolean isDetect() {
        return this.code > 0 && this.code < 100;
    }

    public boolean isText() {
        return this.code >= 100 && this.code < 200;
    }

    public boolean isBytes() {
        return this.code >= 200 && this.code < 300;
    }

    public int getCode() {
        return code;
    }
}
