package cn.cns.socket.ws;

import cn.cns.socket.ws.enums.WsBizBaseType;
import cn.cns.socket.ws.enums.WsBizType;

import jakarta.websocket.*;

public class WebSocketEndPoint {
    static {
        WsBizType.load(WsBizBaseType.class);
    }

    @OnOpen
    public void onOpen(Session session) {
        WsUtils.onOpen(session);
    }

    @OnMessage
    public void onMessage(Session session, WsMessage msgIn) {
        switch (msgIn.getDataType()) {
            case PING:
                WsUtils.sendToSession(session, WsMessage.PONG);
                break;
            case PONG:
                break;
            default:
                WsUtils.onMessage(session, msgIn);
                break;
        }
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        WsUtils.onClose(session, reason);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        WsUtils.onError(session, throwable);
    }
}
