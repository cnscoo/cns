package cn.cns.socket.ws.convert;

import cn.cns.socket.ws.WsMessage;
import cn.cns.socket.ws.enums.WsBizType;
import cn.cns.socket.ws.enums.WsDataType;
import lombok.extern.slf4j.Slf4j;

import jakarta.websocket.Decoder;
import jakarta.websocket.EndpointConfig;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * @author dhc
 */
@Slf4j
public class MsgDecoder implements Decoder.Binary<WsMessage> {
    @Override
    public WsMessage decode(ByteBuffer bytes) {
        try {
            bytes.position(0);
            WsBizType bizType = WsBizType.VALUES.get(bytes.getInt());
            WsDataType msgType = WsDataType.of(bytes.getInt());
            if (bizType == null || msgType == null) {
                return null;
            }
            WsMessage message = new WsMessage();
            message.setBizType(bizType);
            message.setDataType(msgType);
            if (msgType.isDetect() || bytes.capacity() <= 4) {
                return message;
            }
            int length = bytes.getInt();
            if (length <= 0) {
                return message;
            }
            byte[] data = new byte[length];
            bytes.get(data);
            if (msgType.isText()) {
                message.setData(new String(data, StandardCharsets.UTF_8));
            } else if (msgType.isBytes()) {
                message.setData(data);
            }
            return message;
        } catch (Exception e) {
            log.warn("WSC >>> 解析消息出错：{}", e.getMessage());
            bytes.position(0);
            int length = bytes.limit();
            if (length > 0) {
                byte[] data = new byte[length];
                bytes.get(data);
                WsMessage message = new WsMessage();
                message.setDataType(WsDataType.TEXT);
                message.setData(new String(data, StandardCharsets.UTF_8));
                return message;
            }
            return null;
        }
    }

    @Override
    public boolean willDecode(ByteBuffer bytes) {
        return bytes.capacity() >= 4;
    }

    @Override
    public void init(EndpointConfig config) {

    }

    @Override
    public void destroy() {

    }
}
