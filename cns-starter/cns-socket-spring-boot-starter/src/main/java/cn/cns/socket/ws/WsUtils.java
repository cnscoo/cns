package cn.cns.socket.ws;

import cn.cns.socket.ws.enums.WsBizType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.websocket.CloseReason;
import jakarta.websocket.EncodeException;
import jakarta.websocket.Session;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;

public class WsUtils {
    private static final Logger log = LoggerFactory.getLogger(WsUtils.class);

    private static final List<WsEventHandler> HANDLERS = new ArrayList<>();

    public static void addHandler(WsBizType type, BiConsumer<Session, WsMessage> consumer) {
        WsEventHandler handler = WsEventHandler.newBizMessageHandler(type, consumer);
        addHandler(handler);
    }

    public static synchronized void addHandler(WsEventHandler handler) {
        if (handler != null) {
            if (!HANDLERS.contains(handler)) {
                HANDLERS.add(handler);
                if (HANDLERS.size() > 1) {
                    HANDLERS.sort(WsEventHandler::compareTo);
                }
            }

        }
    }

    public static boolean removeHandler(WsEventHandler handler) {
        return HANDLERS.remove(handler);
    }

    public static void onOpen(Session session) {
        HANDLERS.parallelStream().filter(i -> i.supportSession(session)).forEach(i -> i.onOpen(session));
    }

    public static void onClose(Session session, CloseReason reason) {
        HANDLERS.parallelStream().filter(i -> i.supportSession(session)).forEach(i -> i.onClose(session, reason));
    }

    public static void onError(Session session, Throwable thr) {
        HANDLERS.parallelStream().filter(i -> i.supportSession(session)).forEach(i -> i.onError(session, thr));
    }

    public static void onMessage(Session session, WsMessage message) {
        HANDLERS.stream().filter(i -> i.supportMessage(session, message)).forEach(i -> i.onMessage(session, message));
    }

    public static void sendToSession(Session session, WsMessage result) {
        sendToSessions(Collections.singletonList(session), result);
    }

    public static void sendToSessions(Collection<Session> sessions, WsMessage result) {
        sessions.parallelStream().filter(Session::isOpen).forEach(s -> {
            try {
                s.getBasicRemote().sendObject(result);
            } catch (IOException | EncodeException e) {
                log.error("WSC >>> 发送 WebSocket 消息失败", e);
            }
        });
    }
}
