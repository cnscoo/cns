package cn.cns.socket.ws;

import cn.cns.socket.ws.convert.MsgDecoder;
import cn.cns.socket.ws.convert.MsgEncoder;
import cn.cns.socket.ws.enums.WsBizBaseType;
import cn.cns.socket.ws.enums.WsBizType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import jakarta.websocket.server.ServerEndpointConfig;
import java.util.Collections;

@Configuration
public class CnsSocketConfiguration {
    private static boolean websocketEnabled = false;

    @Value("${cns.websocket.url:}")
    private String websocketUrl;

    public static boolean isWebsocketEnabled() {
        return websocketEnabled;
    }

    @Bean
    @ConditionalOnProperty(prefix = "cns", name = "websocket.url")
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }

    @Bean
    @ConditionalOnProperty(prefix = "cns", name = "websocket.url")
    public ServerEndpointConfig serverEndpointConfig() {
        WsBizType.load(WsBizBaseType.class);
        ServerEndpointConfig config = ServerEndpointConfig.Builder
                .create(WebSocketEndPoint.class, websocketUrl)
                .encoders(Collections.singletonList(MsgEncoder.class))
                .decoders(Collections.singletonList(MsgDecoder.class))
                .build();
        websocketEnabled = true;
        return config;
    }
}
