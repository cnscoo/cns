package cn.cns.socket.ws;

import cn.cns.socket.ws.enums.WsBizBaseType;
import cn.cns.socket.ws.enums.WsBizType;
import cn.cns.socket.ws.enums.WsDataType;
import cn.cns.web.utils.BeanCache;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;

import java.io.Serializable;

public class WsMessage implements Serializable {
    public static WsMessage PING = of(WsBizBaseType.DETECT, WsDataType.PING);
    public static WsMessage PONG = of(WsBizBaseType.DETECT, WsDataType.PONG);

    private WsBizType bizType;
    private WsDataType dataType;
    private Object data;

    public WsMessage() {
        this.bizType = WsBizBaseType.OTHER;
        this.dataType = WsDataType.TEXT;
    }

    public WsMessage(@NonNull WsBizType bizType, @NonNull WsDataType dataType) {
        this.bizType = bizType;
        this.dataType = dataType;
    }

    public WsMessage(@NonNull WsBizType bizType, @NonNull WsDataType dataType, Object data) {
        this.bizType = bizType;
        this.dataType = dataType;
        this.data = data;
    }

    public WsBizType getBizType() {
        return bizType;
    }

    public void setBizType(@NonNull WsBizType bizType) {
        this.bizType = bizType;
    }

    public WsDataType getDataType() {
        return dataType;
    }

    public void setDataType(@NonNull WsDataType dataType) {
        this.dataType = dataType;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


    public String stringData() {
        if (this.getData() instanceof String) {
            return (String) this.getData();
        }
        return this.getData() != null ? this.getData().toString() : null;
    }

    private ObjectMapper mapper() {
        if (this.getDataType() != null) {
            switch (this.getDataType()) {
                case JSON:
                    return BeanCache.getJsonMapper();
                case XML:
                    return BeanCache.getXmlMapper();
                default:
                    return null;
            }
        }
        return null;
    }

    public <T> T objectData(Class<T> clz) {
        ObjectMapper mapper;
        if ((mapper = mapper()) != null && this.getData() instanceof String) {
            try {
                return mapper.readValue((String) this.getData(), clz);
            } catch (JsonProcessingException ignored) {
            }
        }
        return null;
    }

    public <T> T objectData(TypeReference<T> type) {
        ObjectMapper mapper;
        if ((mapper = mapper()) != null && this.getData() instanceof String) {
            try {
                return mapper.readValue((String) this.getData(), type);
            } catch (JsonProcessingException ignored) {
            }
        }
        return null;
    }


    public static <T> WsMessage of(@NonNull WsBizType bizType, @NonNull WsDataType dataType, T content) {
        Assert.notNull(bizType, "WebSocket message's business type can't be null.");
        Assert.notNull(dataType, "WebSocket message's data type can't be null.");
        return new WsMessage(bizType, dataType, content);
    }

    public static <T> WsMessage of(@NonNull WsBizType bizType, @NonNull WsDataType dataType) {
        return of(bizType, dataType, null);
    }

    public static <T> WsMessage ofJson(@NonNull WsBizType bizType, @NonNull T content) {
        return of(bizType, WsDataType.JSON, content);
    }

    public static <T> WsMessage ofText(@NonNull WsBizType bizType, @NonNull T content) {
        return of(bizType, WsDataType.TEXT, content);
    }
}
