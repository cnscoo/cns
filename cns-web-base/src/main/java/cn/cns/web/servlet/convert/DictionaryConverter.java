package cn.cns.web.servlet.convert;

import cn.cns.common.dto.IDictionary;
import lombok.SneakyThrows;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.validation.BindException;

import java.util.Arrays;

public class DictionaryConverter<T extends IDictionary<?>> implements Converter<String, T> {
    private final Class<T> clz;
    private final boolean enable;

    public DictionaryConverter(Class<T> clz) {
        this.clz = clz;
        this.enable = clz.isEnum();
    }

    @Override
    @SneakyThrows
    public T convert(@NonNull String s) {
        if (!enable) {
            return null;
        }
        return Arrays.stream(clz.getEnumConstants())
                .filter(i -> i.getValue().toString().equalsIgnoreCase(s))
                .findFirst()
                .orElseThrow(() -> new BindException(s, clz.getName()));
    }
}
