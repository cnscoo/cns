package cn.cns.web.servlet.convert;

import cn.cns.common.dto.IDictionary;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unchecked", "rawtypes"})
public class DictionaryConverterFactory implements ConverterFactory<String, IDictionary<?>> {
    private final Map<Class<?>, Converter> converters = new HashMap<>();

    @Override
    public <T extends IDictionary<?>> Converter<String, T> getConverter(Class<T> targetType) {
        if (!converters.containsKey(targetType)) {
            DictionaryConverter<T> converter = new DictionaryConverter<>(targetType);
            converters.put(targetType, converter);
            return converter;
        }
        return converters.get(targetType);
    }
}
