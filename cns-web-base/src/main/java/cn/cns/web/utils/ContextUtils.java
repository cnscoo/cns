package cn.cns.web.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author dhc
 * 2019-11-22 21:47
 */
@Slf4j
public final class ContextUtils {
    public static ConfigurableApplicationContext context;

    public ContextUtils(ConfigurableApplicationContext context) {
        ContextUtils.context = context;
    }

    /**
     * 简单的注册 Bean
     *
     * @param clz             Bean 的 Class 类型
     * @param constructorArgs 构造方法参数
     * @param <T>             Bean 的类型
     * @return 是否注册成功
     */
    public static <T> boolean registBean(Class<T> clz, Object... constructorArgs) {
        int size = context.getBeansOfType(clz).size();
        if (size > 0) {
            log.warn("Bean 类型 {} 已经存在 {} 个，注册失败", clz.getName(), size);
            return false;
        }
        String className = clz.getSimpleName();
        String beanName = className.substring(0, 1).toLowerCase() + className.substring(1);
        return registBean(clz, beanName, constructorArgs);
    }


    /**
     * 简单的注册 Bean
     *
     * @param clz             Bean 的 Class 类型
     * @param beanName        Bean 的名称
     * @param constructorArgs 构造方法参数
     * @param <T>             Bean 的类型
     * @return 是否注册成功
     */
    public static <T> boolean registBean(Class<T> clz, String beanName, Object... constructorArgs) {
        if (context.containsBean(beanName)) {
            log.warn("名字为 {} 的 Bean 已经存在，注册失败", beanName);
            return false;
        }
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(clz);
        Arrays.stream(constructorArgs).forEach(beanDefinitionBuilder::addConstructorArgValue);
        BeanDefinition beanDefinition = beanDefinitionBuilder.getRawBeanDefinition();
        BeanDefinitionRegistry beanDefinitionRegistry = (BeanDefinitionRegistry) context.getBeanFactory();
        beanDefinitionRegistry.registerBeanDefinition(beanName, beanDefinition);
        return true;
    }

    /**
     * 根据类型获取bean，有多项时，返回类名相同的项，否则返回第一项
     *
     * @param clazz bean类型
     * @param <T>   bean泛型
     * @return Bean
     */
    public static <T> T getBean(Class<T> clazz) {
        try {
            Collection<T> beans = context.getBeansOfType(clazz).values();
            return beans.isEmpty() ? null : beans.stream().filter(t -> t.getClass().equals(clazz)).findFirst().orElse(beans.iterator().next());
        } catch (Exception e) {
            return null;
        }
    }
}
