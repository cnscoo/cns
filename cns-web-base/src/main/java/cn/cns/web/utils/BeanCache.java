package cn.cns.web.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

/**
 * 静态 Bean 引用类
 *
 * @author dhc
 */
public class BeanCache {
    private static final Map<Class<?>, Object> CACHE = new ConcurrentHashMap<>();

    public static void configObjectMapperDefault(ObjectMapper objectMapper) {
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
        objectMapper.registerModule(new JavaTimeModule());
    }

    public static <T extends ObjectMapper> T configObjectMapperDefault(Supplier<T> supplier) {
        T mapper = supplier.get();
        configObjectMapperDefault(mapper);
        return mapper;
    }

    @SuppressWarnings("unchecked")
    public static <T> T getIfAbsent(Class<T> clz, Supplier<T> supplier) {
        return (T) CACHE.computeIfAbsent(clz, c -> supplier.get());
    }

    public static <T> T getBeanIfAbsent(Class<T> clz, Supplier<T> supplier) {
        return getIfAbsent(clz, () -> Optional.ofNullable(ContextUtils.getBean(clz)).orElseGet(supplier));
    }

    public static JsonMapper getJsonMapper() {
        return getBeanIfAbsent(JsonMapper.class, () -> configObjectMapperDefault(JsonMapper::new));
    }

    public static XmlMapper getXmlMapper() {
        return getBeanIfAbsent(XmlMapper.class, () -> configObjectMapperDefault(XmlMapper::new));
    }

    public static RestTemplate getRestTemplate() {
        return getBeanIfAbsent(RestTemplate.class, () -> {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().stream()
                    .filter(c -> c instanceof MappingJackson2HttpMessageConverter)
                    .map(c -> (MappingJackson2HttpMessageConverter) c)
                    .forEach(c -> {
                        List<MediaType> supportTypes = new ArrayList<>(c.getSupportedMediaTypes());
                        supportTypes.add(MediaType.TEXT_PLAIN);
                        c.setSupportedMediaTypes(supportTypes);
                    });
            return restTemplate;
        });
    }
}
