package cn.cns.web.config;

import cn.cns.web.servlet.convert.DictionaryConverterFactory;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

public class WebBaseConfiguration implements WebMvcConfigurer {

    // 添加枚举转换
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverterFactory(new DictionaryConverterFactory());
    }
}
