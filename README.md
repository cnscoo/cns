# Cnscoo

## cns-common

常用的通用类和工具类。

## cns-web-base

集成一些Web的基本资源操作。

## cns-wechat-spring-boot-starter

快速集成微信公众号和公众平台的各项操作。
